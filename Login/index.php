<?php
$url = "../";
$name = "Login";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html lang="pt">

    <head>
        <?php head($url, $name); ?>
    </head>

    <body>
        <?php loader(); ?>

        <?php menu($url, $name); ?>

        <!-- TITLE SECTION -->
        <section class="page-header clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1>login</h1> 
                        <div class="line-shape line-shape-center"></div> 
                    </div>
                </div>
            </div>
        </section>
        <!-- / TITLE SECTION -->

        <section class="special-area bg-white section_padding_50" id="section-login">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-4">
                        <div class="icon-login text-center" style="color:#02ea02;padding:0px 0 30px">
                            <span class='fa fa-user fa-4x'></span>
                        </div>
                        <div class="form-group">
                            <label>Usuario / E-mail</label>
                            <input type="email" class="form-control" id="loginUser" placeholder="Ingrese su usuario o email" required="" autocomplete="off">
                            <div class="div-validador">
                                <div>&nbsp;</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Senha</label>
                            <input type="password" class="form-control" id="passwordUser" placeholder="Ingrese su Clave" required="">
                            <div class="div-validador">
                                <div>&nbsp;</div>
                            </div>
                        </div>
                        <button id="sendLogin" type="submit" class="btn submit-btn mt-3">Ingresar</button>
                        <div class="form-group mt-5">
                            <span>¿Aun no tienes cuenta? <a href="../Register/">Registrate</a></span>
                            <p><span>¿No recuerdas tu contraseña? <a href="recovery_pass.php">Recuperar</a></span><p>
                        </div>
                    </div>
                </div>
        </section>

        <?php footer($url); ?>

        <?php scripts($url); ?>
        <script src="../lib/common/js/validate.js"></script>
        <script src="../lib/jwt/jwt-decode.min.js"></script>
        <script src="../lib/common/js/login.js"></script>
    </body>

</html>
