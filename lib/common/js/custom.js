// Auto Load
$(document).ready(function () {
    $(".logout").on("click", function () {
        logout();
    });
});

// WEB VALID
function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'), sParameterName, i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function GeneratePop(title, text, type, url, xhr = null) {
    if (xhr != null) {
        type = "error";
        if (xhr.status == 0 || xhr.status == 500) {
            text = "Los servicios no se encuentran disponibles, intente nuevamente en unos minutos. (" + xhr.status + ")";
        } else if (xhr.status == 401 || xhr.status == 403) {
            url = HomeUrl + "/Login";
            title = "Error!";
            text = JSON.parse(xhr.responseText).message + " (" + JSON.parse(xhr.responseText).code + ")";
            localStorage.clear();
        } else {
            text = JSON.parse(xhr.responseText).message + " (" + JSON.parse(xhr.responseText).code + ")";
        }
    }
    swal({
        title: title,
        text: text,
        type: type,
        closeOnConfirm: true,
        allowOutsideClick: true,
        html: true
    }, function (isConfirm) {
        if (url != "no") {
            window.location.href = url;
        }
    });
}

// Loader
function ShowLoader() {
    $("#preloader").show();
}
function HideLoader() {
    setTimeout(function () {
        $("#preloader").fadeOut(500);
    }, 800);
}
function showLoader(tag) {
    tag.show();
}
function hideLoader(tag) {
    setTimeout(function () {
        tag.fadeOut(500);
    }, 800);
}
function logout(url) {
    swal({
        title: "¡Cerrar Sesión!",
        text: '¿Seguro que desea salir de Coins2Pump?',
        imageUrl: url + 'lib/common/img/coins2pump-logo.png',
        imageSize: '150x80',
        imageAlt: 'Custom image',
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        cancelButtonClass: "btn-danger",
        closeOnConfirm: false,
        allowOutsideClick: true
    },
            function () {
                localStorage.clear();
                window.location.href = url;
            });

}
