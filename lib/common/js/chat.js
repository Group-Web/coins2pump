var UrlProfile = HomeUrl + 'Profile/';
var UrlDashboardPay = HomeUrl + 'Dashboard/Payment';
var UrlChat = BaseUrl + 'chat/';
var UrlChatSearch = UrlChat + 'search/';
var UrlGetInChat = UrlChat + 'in/';
var UrlBannUserChat = UrlChat + 'bann/';
var UrlDetailChat = UrlChat + 'detail/';
var UrlValidUser = UrlChat + 'validate/';
var UrlCoupon = UrlChat + 'coupon/';
var SelfUsername = '';
var flipClockElems = [];

function topicSlug(channel) {
    return ['app/chat', channel].join('/');
}
function subscribeHandler(uri, payload) {
    if (payload.oldMsg && payload.username == SelfUsername) {
        payload.oldMsg = $.parseJSON(payload.oldMsg);
        $.each(payload.oldMsg, function (i, payload) {
            Chat.appendMessage(payload);
        });
    }
    console.log('Received message: ', uri, payload);
    Chat.appendMessage(payload);
}

var Chat = {
    connectSession: function (item) {
        this.session;
        this.session.unsubscribe(topicSlug(item.id), subscribeHandler);
        this.connectChannel(this.session, item.id);
    },
    init: function () {
        var that = this;
        this.session = 'asd';
        var webSocket = WS.connect(WebSocketUrl + "?t=" + localStorage['coins_token']);
        webSocket.on("socket/connect", function (session) {
            //session is an Autobahn JS WAMP session.
            that.request(session);
            that.session = session;
            $('#chats-container').on('click', '#removeChat', function (event) {
                event.preventDefault();
                var remove = $(this).data('remove');
                $('#chats-container #' + remove).fadeOut('slow', function () {
                    $(this).remove();
                });
                if (session._subscriptions[topicSlug(remove)]) {
                  session.unsubscribe(topicSlug(remove), subscribeHandler);
                }
            });
            console.log("Successfully Connected!");
        });
        webSocket.on("socket/disconnect", function(error){
          //error provides us with some insight into the disconnection: error.reason and error.code
          console.log("Disconnected for " + error.reason + " with code " + error.code);
          //alert('Chat Server Connection Lost ... ');
          GeneratePop('Lo sentimos ...' , "El servicio de Chat no se encuentra disponible", "error" , HomeUrl+'Dashboard');
        });

        $('.container').on('show.bs.collapse', function (e) {
            $('.script-container').each(function (index, el) {
                $(this).html('');
            });
            setTimeout(function () {
                if ($('#chats-container').find('.collapse.show').find('.script-container').length) {
                    that.addConclusionWidget($('#chats-container').find('.collapse.show').find('.script-container'));
                }
            }, 500);
            $('.collapse.show').collapse('hide');
            $(this).find('.badge.bg-light-green').fadeOut('slow').html('0');
        });
        $('.container').on('click', '.direct-chat-messages', function (event) {
            event.preventDefault();
            $(this).closest('.direct-chat').find('.badge.bg-light-green').fadeOut('slow').html('0');
        });

        $('#chats-container').on('click', '#connected-users', function (event) {
            event.preventDefault();
            var list = $(this).closest('.direct-chat').find('.direct-chat-contacts');
            if (list.attr('style') == undefined) {
                list.css('transform', 'translate(0%,0)');
            } else {
                list.removeAttr('style');
            }
        });

        $('.container').on('click', '#light-bann', function (event) {
            that.bannUser($(this).data('ban'), $(this).closest('.contacts-list').data('chat'), $(this));
        });

        $('.container').on('click', '#light-coupon', function (event) {
            that.useCoupon($(this).data('coupon'), $(this).closest('.coupon-list').data('chat'), $(this));
        });
    },
    request: function (session) {
        var that = this;
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            headers: {"Authorization": localStorage['coins_token']}, //localStorage['coins_token']
            type: "GET",
            url: UrlChat,
            beforeSend: function (xhr) {
              ShowLoader();
            },
            success: function (result) {
                SelfUsername = result.login;
                var data = result.data;
                $.each(data, function (i, item) {
                    if (item.isvalidChat == true) {
                        $('#emptyRoom').hide();
                        that.structureChat(item, $('#chats-container'));
                        if ((item.isowner == 1 || item.isvalid == 1 || item.type == 'free') && ((new Date(item.start).getTime() < new Date(item.time).getTime()) && (new Date(item.end).getTime() > new Date(item.time).getTime()))) {
                            that.connectChannel(session, item.id);
                        }
                    }
                });

                $('#chats-container').on('click', '.box-footer .input-group button', function (event) {
                    event.preventDefault();
                    var input = $(this).parent().prev();
                    var channel = input.data('chat');
                    session.publish(topicSlug(channel), input.val().trim());
                    input.val('');

                    var scrollable = $(this).closest('.collapse.show').find('.direct-chat-messages');
                    setTimeout(function () {
                      scrollable.animate({scrollTop: scrollable.prop('scrollHeight')});
                    },1000);
    		        });


                $('#chats-container').on('keyup', '.box-footer .input-group textarea', function (event) {
                    event.preventDefault();
                    if (event.keyCode == 13) {
                        var input = $(this);
                        var channel = input.data('chat');
                        session.publish(topicSlug(channel), input.val().trim());
                        input.val('');

                        var scrollable = $(this).closest('.collapse.show').find('.direct-chat-messages');
                        setTimeout(function () {
                            scrollable.animate({scrollTop: scrollable.prop('scrollHeight')});
                        }, 1000);
                    }
                });
            },
            error: function (xhr) {
                GeneratePop(xhr.responseJSON.message, "", "error", "no", xhr);
            },
            complete: function (jqXHR, textStatus) {
              HideLoader();
            }
        });
    },
    structureChat: function (item, appendTo) {
        var that = this;
        var chatBox = '';
        var descriptionInfo = '';
        var descriptionResponsive = '';
        var chatInput = '<div class="col-md-12"></div>';
        if ($('#chats-container #' + item.id).length || (item.isowner != 1 && item.isvalid != 1 && item.type != 'free' && item.status != 'Activo')) {
            return '';
        }
        if (item.type == 'premium') {
            var faicon = '<span class="premium"><i class="fas fa-certificate"></i> Premium</span>';
        } else {
            var faicon = '<span class="free"><i class="fas fa-comments"></i> Free</span>';
        }

		    if ( (new Date(item.time).getTime() > new Date(item.end).getTime()) || (item.conclusion != null) ) {
              descriptionInfo = '<div class="col-md-12 info-body">' +
                                  '<div class="row">' +
                                    '<div class="col-md-3">' +
                                      '<div class="description text-center" style="background: #cecece;">' +
                                         '<div class="user row" style="background: transparent; border: 0px;">' +
                                            '<div class="user-img col-md-10 offset-md-1">' +
                                                '<img src="'+item.userphoto+'" alt="User image">' +
                                            '</div>' +
                                            '<div class="user-info col-md-12 align-self-center" style="font-size:  15px;">' +
                                                '<h4>'+item.user+'</h4>' +
                                                '<small>'+item.descriptionUser+'</small><br>' +
                                                '<a href="'+UrlProfile+'?user='+item.user+'" target="_blank">'+'Watch my Video Blog!'+'</a>' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="description conclusion text-center">' +
                                          '<h4>'+item.conclusion.currency+' ('+item.conclusion.currencyCode+')</h4>' +
                                          '<img src="'+item.conclusion.currencyImg+'">'+
                                          //'<p class="text-justify">'+item.conclusion+'</p>'+
                                        '</div>' +
                                        '<h4>Topics</h4>'+
                                        '<p class="text-justify">'+item.description+'</p>'+
                                      '</div>' +
                                    '</div>' +
                                    '<div class="col-md-9">' +
                                      '<div id="preloader2" style="position: absolute;">' +
                                           '<div class="colorlib-load"></div>' +
                                       '</div>' +
                                      '<div class="script-container" data-cryp="'+item.conclusion.currencyCode+'"></div>' +
                                    '</div>' +
                                  '</div>' +
                                '</div>';
        }else if ( new Date(item.time).getTime() < new Date(item.start).getTime() ) {

              descriptionInfo = '<div class="col-md-6 info-body">' +
                                  '<div class="description text-center">' +
                                     '<div class="user row">' +
                                        '<div class="user-img col-md-3">' +
                                            '<img src="'+item.userphoto+'" alt="User image">' +
                                        '</div>' +
                                        '<div class="user-info col-md-9 align-self-center">' +
                                            '<h4>'+item.user+'</h4>' +
                                            '<small>'+item.descriptionUser+'</small><br>' +
                                            '<a href="'+UrlProfile+'?user='+item.user+'" target="_blank">'+'Watch my Video Blog!'+'</a>' +
                                        '</div>' +
                                      '</div>' +
                                    '<h4>Topics</h4>'+
                                    '<p class="text-justify">'+item.description+'</p>'+
                                  '</div>' +
                                '</div>'+
                                '<div class="col-md-6 info-body">' +
                                    '<div class="text-center" id="conclusion">' +
                                    '<h4>Start in:</h4>'+
                                    '<div class="clock text-center">'+
                                      '<div id="clock-'+item.id+'" style="padding:30px 0!important;margin: 0 auto!important;"></div>'+
                                    '</div>'+
                                  '</div>' +
                                '</div>';

        } else if ( (new Date(item.start).getTime() < new Date(item.time).getTime()) && (new Date(item.end).getTime() > new Date(item.time).getTime()) ) {
            	descriptionResponsive = '<div class="d-sm-none d-xs-block col-12 info-body p-0" style="height:auto;">' +
                                        '<div class="description text-center p-0">' +
                                           '<div class="user row m-0">' +
                                              '<!--<div class="user-img col-4">' +
                                                  '<img src="'+item.userphoto+'" alt="User image">' +
                                              '</div>' +
                                              '<div class="col-8">' +
                                                  '<h4>'+item.user+'</h4>' +
                                              '</div>-->' +
                                              '<div class="col-md-12">' +
                                                  '<h4>'+item.user+'</h4>' +
                                                  '<small>'+item.descriptionUser+'</small><br>' +
                                                  '<a href="'+UrlProfile+'?user='+item.user+'" target="_blank">'+'Watch my Video Blog!'+'</a>' +
                                              '</div>' +
                                            '</div>' +
                                        '</div>' +
                                      '' +
                                      '</div>';

              chatBox = '<div class="box-body col-md-6">'+
                          '<!-- Conversations are loaded here -->'+
                          '<div class="direct-chat-messages">'+
                            '' +
                          '</div>'+
                          '<!--/.direct-chat-messages-->'+
                          '<!-- Contacts are loaded here -->'+
                          '<div class="direct-chat-contacts">'+
                            '<div class="title-container">'+
                              '<h3 class="title">Coupon</h3>'+
                              '<hr/>'+
                              '<ul class="coupon-list" data-chat="'+item.id+'">'+
                                '<a href="'+HomeUrl+'Coupon">' +
                                  'Buy Coupon' +
                                '</a>' +
                              '</ul>'+
                              '<hr/>'+
                              '<h3 class="title">Registered Users</h3>'+
                              '<hr>'+
                            '</div>'+
                            '<ul class="contacts-list" data-chat="'+item.id+'">'+
                              
                              '<!-- End Contact Item -->'+
                            '</ul>'+
                            '<!-- /.contatcts-list -->'+
                          '</div>'+
                          '<!-- /.direct-chat-pane -->'+
                        '</div>';

              descriptionInfo = '<div class="d-none d-sm-block col-md-6 info-body">' +
                                  '<div class="description text-center">' +
                                     '<div class="user row">' +
                                        '<div class="user-img col-md-3">' +
                                            '<img src="'+item.userphoto+'" alt="User image">' +
                                        '</div>' +
                                        '<div class="user-info col-md-9 align-self-center">' +
                                            '<h4>'+item.user+'</h4>' +
                                            '<small>'+item.descriptionUser+'</small><br>' +
                                            '<a href="'+UrlProfile+'?user='+item.user+'" target="_blank">'+'Watch my Video Blog!'+'</a>' +
                                        '</div>' +
                                    '</div>' +
                                    '<h4>Topics</h4>'+
                                    '<p class="text-justify">'+item.description+'</p>'+
                                  '</div>' +
                                  '<div class="text-center" id="conclusion">' +
                                    '<h4>Finish:</h4>'+
                                    '<div class="clock text-center">'+
                                      '<div id="clock-'+item.id+'" data-end="'+item.end+'" style="padding:30px 0!important;margin: 0 auto!important;"></div>'+
                                    '</div>'+
                                  '</div>' +
                                '</div>';
              chatInput = '<div class="col-md-6 box-footer">'+
                              '<div class="input-group">'+
                                '<textarea rows="1" id="'+item.id+'" data-chat="'+item.id+'" type="text" name="message" placeholder="Type Message ..." class="form-control"></textarea>'+
                                '<span class="input-group-btn">'+
                                  '<button id="'+item.id+'" data-chat="'+item.id+'" class="btn btn-primary btn-flat">Send</button>'+
                                '</span>'+
                              '</div>'+
                          '</div>'+
                          '<div class="col-md-6">'+
                          '</div>';
        }
    
        appendTo.append("<div id='"+item.id+"' data-chat='"+JSON.stringify(item)+"' class='col-md-12'>" +
                          '<!-- DIRECT CHAT PRIMARY -->'+
                          '<div class="box box-'+item.type+' direct-chat direct-chat-coins">'+
                            '<div class="box-header with-border p-1">'+
                              '<div class="title-container collapsed">'+
                                '<div class="container">' +
                                  '<div class="row">' +
                                    '<div class="col-3 text-left p-0">' +
                                        faicon +
                                    '</div>' +
                                    '<div class="col-6 p-0" data-toggle="collapse" data-owner="'+item.user+'" href="#tab'+item.id+'" aria-expanded="false" aria-controls="tab'+item.id+'">' +
                                        '<h3 class="box-title">'+item.name+'</h3>'+
                                    '</div>' +
                                    '<div class="col-3 text-right p-0">' +
                                      '<div class="box-tools pull-right">'+
                                        '<span data-toggle="tooltip" title="New Messages" class="badge bg-light-green" data-original-title="New Messages" style="display:none;">0</span>'+
                                        '<button type="button" class="btn btn-box-tool" data-toggle="collapse" href="#tab'+item.id+'" aria-expanded="true" aria-controls="tab'+item.id+'"><i class="fa fa-minus"></i>'+
                                        '</button>'+
                                        '<button type="button" id="connected-users" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">'+
                                          '<i class="fa fa-list-ul"></i></button>'+
                                        '<button type="button" class="btn btn-box-tool" id="removeChat" data-remove="'+item.id+'"><i class="fa fa-times"></i></button>'+
                                      '</div>'+
                                    '</div>' +
                                  '</div>' +
                                '</div>' +
                              '</div>'+
                            '</div>'+
                            '<!-- /.box-header -->'+
                            '<div class="container collapse" id="tab'+item.id+'">'+
                                '<div class="row">'+
                                    descriptionResponsive +
                                    chatBox +
                                    descriptionInfo +
                                '</div>'+
                                '<!-- /.box-body -->'+
                                '<div class="row">'+
                                    chatInput +
                                '</div>'+
                                '<!-- /.box-footer-->'+
                            '</div>'+
                          '</div>'+
                          '<!--/.direct-chat -->'+
                        '</div>');
    		if ( new Date(item.start).getTime() > new Date(item.time).getTime() ) {
    			var flipClockDate = Math.round(Math.abs(new Date(item.start) - new Date(item.time).getTime())/1000);
          flipClockElems['clock-'+item.id] =  $('#clock-'+item.id).FlipClock(flipClockDate, {
                                    	            clockFace: 'DailyCounter',
                                    	            countdown: true,
                                    	            autoStart: true,
                                    	            callbacks: {
                                    		        	stop: function() {
                                    		        		that.requestDetails(item);
                                    		        	}
                                    		        }
                                    	        });
    		} else if ( (new Date(item.start).getTime() < new Date(item.time).getTime()) && (new Date(item.end).getTime() > new Date(item.time).getTime()) ) {
    			var flipClockDate = Math.round(Math.abs(new Date(item.end) - new Date(item.time).getTime())/1000);
    	    flipClockElems['clock-'+item.id] =  $('#clock-'+item.id).FlipClock(flipClockDate, {
                                    	            clockFace: 'DailyCounter',
                                    	            countdown: true,
                                    	            autoStart: true,
                                    	            callbacks: {
                                    		        	stop: function() {
                                            				that.requestDetails(item);
                                    		        	}
                                    		        }
                                    	        });
    		}

        this.contactList(item.id, item.userList);
        this.couponList(item.id, item.coupon);
        return true;
    },
    connectChannel: function (session, channel) {
        console.log('Connecting');
        session.subscribe(topicSlug(channel), subscribeHandler);
    },
    appendMessage: function (payload) {
        console.log(payload);
        var tab = $("#tab" + payload.channel + '*');
        var tabHeader = tab.prev();
        var owner = tabHeader.find('.title-container').data('owner');
        var directChatMessages = $("#tab" + payload.channel + " .direct-chat-messages*");
        var time = new Date();
        time = time.getHours() + ':' + time.getMinutes() + ':' + time.getSeconds();

        if ((payload.type == 'connection') || (payload.type == 'message')) {
            $('.contacts-list #' + payload.username).find('.fa-lightbulb').removeClass('text-discconnected').addClass('text-success');
        } else {
            $('.contacts-list #' + payload.username).find('.fa-lightbulb').removeClass('text-success').addClass('text-discconnected');
        }
        if (payload.type == 'connection') {
            var newMsg = '<div class="direct-chat-msg">' +
                    '<div class="direct-chat-info clearfix">' +
                    '<span class="direct-chat-name pull-left">' + payload.username + ' se ha Conectado </span> ' +
                    '<span class="direct-chat-timestamp pull-right"> ' + time + '</span>' +
                    '</div>' +
                    '</div>';
        } else if (payload.type == 'failconnection') {
            var newMsg = '<div class="direct-chat-msg">' +
                    '<div class="direct-chat-info clearfix text-center">' +
                    '<span class="direct-chat-name"> Chat Error </span>' +
                    '</div>' +
                    '</div>';
        } else if (payload.type == 'message') {

            if (payload.username != SelfUsername) {
                var tabBadge = tabHeader.find('.badge.bg-light-green');
                tabBadge.fadeIn('slow', function () {
                    tabBadge.html(parseInt(tabBadge.html()) + 1);
                });
            }
            var msgClass = 'right';
            if (payload.username == owner && payload.username != SelfUsername) {
                var msgClass = 'right owner';
            } else if (payload.username == owner && payload.username == SelfUsername) {
                var msgClass = 'owner';
            } else if (payload.username == SelfUsername) {
                var msgClass = '';
            }
            var newMsg = '<div class="direct-chat-msg ' + msgClass + '">' +
                    '<div class="direct-chat-info clearfix">' +
                    '<span class="direct-chat-name pull-left">' + payload.username + '</span>' +
                    '<span class="direct-chat-timestamp pull-right">&nbsp; &nbsp;' + time + '</span>' +
                    '</div>' +
                    '<!-- /.direct-chat-info -->' +
                    '<img class="direct-chat-img" src="https://bootdey.com/img/Content/user_1.jpg" alt="Message User Image"><!-- /.direct-chat-img -->' +
                    '<div class="direct-chat-text">' +
                    payload.body +
                    '</div>' +
                    '<!-- /.direct-chat-text -->' +
                    '</div>';
        }
        directChatMessages.append(newMsg);
    },
    requestDetails: function (item) {
        var that = this;
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            headers: {"Authorization": localStorage['coins_token']},
            type: "GET",
            url: UrlDetailChat + item.id + '/',
            beforeSend: function (xhr) {
            },
            success: function (result) {
                var data = result.data;
                $.each(data, function (i, item) {
                    if (item.isvalidChat == true) {
                      $('#'+item.id+' .container.collapse').collapse('hide');
                      $('#'+item.id).remove();
                      that.structureChat(item, $('#chats-container'));
                      if ((item.isowner == 1 || item.isvalid == 1 || item.type == 'free') && ((new Date(item.start).getTime() < new Date(item.time).getTime()) && (new Date(item.end).getTime() > new Date(item.time).getTime()))) {
                        that.connectSession(item);
                      }
                      $('#'+item.id+' .container.collapse').collapse('show');
                    }
                });

            },
            error: function (xhr) {
                GeneratePop(xhr.responseJSON.data, "", "error", "no", xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    },
    contactList: function (id, userList) {
        var that = this;
        $.each(userList, function (i, item) {
            var ban = '';
            if (item.banned == true) {
                var ban = 'text-warning';
            }
            $('#tab' + id + ' .direct-chat-contacts .contacts-list').append(
                    '<li id="' + item.username + '">' +
                    '<img class="contacts-list-img" src="https://bootdey.com/img/Content/user_1.jpg">' +
                    '<div class="contacts-list-info">' +
                    '<span class="contacts-list-name text-left">' + item.username + '</span>' +
                    '<span id="light-bann" data-toggle="tooltip" title="Click to Bann" class="text-discconnected fa fa-lightbulb ' + ban + '" data-ban="' + item.username + '" id=""></span>' +
                    '</div>' +
                    '</li>'
                    );
        });
    },
    couponList: function (id, couponList) {
        $.each(couponList, function (i, item) {
            var elem = $('#tab' + id + ' .direct-chat-contacts .coupon-list').find('li[data-service="' + item.idServicePayment + '"]');
            if (elem.length == 0) {
                $('#tab' + id + ' .direct-chat-contacts .coupon-list').prepend(
                        '<li data-service="' + item.idServicePayment + '">' +
                        '<div class="contacts-list-info" style="margin: 0px 25px;">' +
                        '<span class="contacts-list-name text-left">' + item.nameServicePayment + '</span>' +
                        '<span id="light-coupon" data-toggle="tooltip" title="Click to Use" class="fa fa-circle text-discconnected" data-coupon="' + item.idServicePayment + '"></span>' +
                        '</div>' +
                        '</li>'
                        );
            }
            if (item.idChat == id) {
                $('#tab' + id + ' .direct-chat-contacts .coupon-list').find('li[data-service="' + item.idServicePayment + '"]').find('.fa-circle').addClass('text-success');
            }



        });
    },
    bannUser: function (username, chat, el) {

        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({loginUser: username, idChat: chat}),
            headers: {"Authorization": localStorage['coins_token']},
            type: "PUT",
            url: UrlBannUserChat,
            beforeSend: function (xhr) {
            },
            success: function (result) {
                if (result.data == true) {
                    el.addClass('text-warning');
                } else {
                    el.removeClass('text-warning');
                }
            },
            error: function (xhr) {
                GeneratePop(xhr.responseJSON.message, "", "error", "no", xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    },
    useCoupon: function (coupon, chat, el) {
      var that = this;
        swal({
            title: 'Confirmar',
            text: '¿Seguro que desea continuar?',
            type: 'success',
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            cancelButtonClass: "btn-danger",
            closeOnConfirm: true,
            allowOutsideClick: true,
            html: true
        }, function (isConfirm) {
            if (isConfirm) {
              $.ajax({
                  async: true,
                  crossDomain: true,
                  contentType: "application/json",
                  dataType: "json",
                  data: JSON.stringify({idServicePayment: coupon, idChat: chat}),
                  headers: {"Authorization": localStorage['coins_token']},
                  type: "PUT",
                  url: UrlCoupon,
                  beforeSend: function (xhr) {
                  },
                  success: function (result) {
                      if (result.data == true) {
                          GeneratePop(result.message, "", "success", "no");
                          el.addClass('text-success');
                          var item = $('#tab'+chat).closest('#'+chat).data('chat');
                          $('#clock-'+item.id).html('');
                          delete flipClockElems['clock-'+item.id];
                          var flipClockDate = Math.round((Math.abs(new Date(item.end) - new Date(item.time).getTime())/1000)-parseInt(result.time));
                          flipClockElems['clock-'+item.id] =  $('#clock-'+item.id).FlipClock(flipClockDate, {
                                                                  clockFace: 'DailyCounter',
                                                                  countdown: true,
                                                                  autoStart: true,
                                                                  callbacks: {
                                                                  stop: function() {
                                                                    that.requestDetails(item);
                                                                  }
                                                                }
                                                              });
                      }
                  },
                  error: function (xhr) {
                      GeneratePop('Error', xhr.responseJSON.message, "error", "no", xhr);
                  },
                  complete: function (jqXHR, textStatus) {
                  }
              });
            }
        });
    },
    addConclusionWidget: function(tag) {
      showLoader(tag.prev('#preloader2'));
      var scripts = tag;
      var divSize = tag.width();
      var cryp = scripts.data('cryp');
      var cccTheme = {"General":{"borderColor":"#5b1966"},"Tabs":{"borderColor":"#5b1966","activeBorderColor":"#5b1966"}};
      var embedder = scripts[ scripts.length - 1 ];
      var s = document.createElement("script");
      s.type = "text/javascript";
      s.async = true;
      if (cryp == 'BTC') {
        var tsym = 'USD';
      }else{
        var tsym = 'BTC';
      }
      if (parseInt(divSize) < 500) {
        s.src = 'https://widgets.cryptocompare.com/serve/v1/coin/chart?fsym='+cryp+'&tsym='+tsym+'&app=coins2pump';
      }else{
        tsym += ',EUR,CNY,GBP';
        s.src = 'https://widgets.cryptocompare.com/serve/v3/coin/chart?fsym='+cryp+'&tsyms='+tsym+'&app=coins2pump';
      }
      embedder.appendChild(s);

      var hideLoaderScript = setInterval(function(){
        if ($('.ccc-widget.ccc-chart-v1')) {
          hideLoader(tag.prev('#preloader2'));
          clearInterval(hideLoaderScript);
        }
      }, 1000);
    },
    flipClock: function(tag) {

    }
};
var Search = {
    init: function () {
        var that = this;
        this.searchButton = $('#searchButton');
        this.input = $('#search');
        this.accordion = $('.modal-body #accordionSearch');

        this.searchButton.on('click', function (event) {
            event.preventDefault();
            var val = $('input#search').val();
            that.request(val);
        });

        $('#freeChatCheck').on('click', function (event) {
            that.filter(this);
        });
        $('#premChatCheck').on('click', function (event) {
            that.filter(this);
        });

        $('#addChat').on('show.bs.modal', function () {
            var val = '*';
            that.request(val);
        });
    },
    request: function (val) {
        var that = this;
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            headers: {"Authorization": localStorage['coins_token']},
            type: "GET",
            url: UrlChatSearch + val,
            beforeSend: function (xhr) {
            },
            success: function (result) {
                var data = result.data;
                that.accordion.html('');
                $('.searchFilter').find('input').attr('checked', 'checked');
                if (!data) {
                    that.accordion.append('<div class="text-center">' +
                            '<h3>Sorry ... No chats found.</h3> ' +
                            '</div>');
                }
                $.each(data, function (i, item) {
                    that.accordion.append(that.itemStructure(item));
                    $('#accordionSearch').on('click', '#addChat' + item.id, function (event) {
                        event.preventDefault();
                        that.getIn(item);
                        $(this).addClass('btn-disabled').attr('disabled', 'disabled').html('Adding ...');
                    });
                });
            },
            error: function (xhr) {
                GeneratePop(xhr.responseJSON.message, "", "error", "no", xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    },
    itemStructure: function (item) {
        var that = this;

        if (item.type == 'premium') {
            var faicon = '<span class="premium"><i class="fas fa-certificate"></i> Premium</span>';
        } else {
            var faicon = '<span class="free"><i class="fas fa-comments"></i> Free</span>';
        }
        if (item.isvalid == 1) {
            var isvalid = '<span class="isvalid"><i class="fas fa-check"></i> Subscribed</span>';
        } else if (item.isowner == 1) {
            var isvalid = '<span class="isvalid"><i class="fas fa-check"></i> Owner</span>';
        } else {
            var isvalid = '';
        }

        var price = '';
        if (item.type != 'free') {
            $.each(item.price, function (i, priceItem) {
                price += '<p>' + priceItem.currency + ': ' + priceItem.value + '</p>';
            });
        } else {
            price = '<p>Free !</p>';
        }
        if (($('#chats-container #' + item.id).length == 0) && ((item.isvalid == 1) || (item.type == 'free') || (item.isowner == 1))) {
            var buttonAdd = '<button class="btn btn-theme small" id="addChat' + item.id + '">Subscribe!</button>';
        } else if (($('#chats-container #' + item.id).length == 0) || ((item.isvalid != 1) && (item.isowner != 1))) {
            var buttonAdd = '<button class="btn btn-theme small" id="addChat' + item.id + '">Buy Now</button>';
            //var buttonAdd = '<a class="btn btn-theme small" href="comprar.php" target="_blank">Buy Now</a>';
        } else if (($('#chats-container #' + item.id).length != 0)) {
            var buttonAdd = '<button class="btn btn-theme btn-disabled small" disabled>Subscribed</button>';
        }
        return '<div class="card ' + item.type + '">' +
                '<div class="card-header " id="' + item.id + '">' +
                //'<h5 class="mb-0">' +
                '<div class="title-container collapsed" data-toggle="collapse" data-target="#collapse' + item.id + '" aria-expanded="false" aria-controls="collapse' + item.id + '">' +
                faicon +
                '<h4 class="box-title">' + item.name + '</h4>&nbsp;' + isvalid +
                '</div>' +
                //'</h5>' +
                '</div>' +
                '<div id="collapse' + item.id + '" class="collapse" aria-labelledby="' + item.id + '" data-parent="#accordionSearch">' +
                '<div class="card-body">' +
                '<div class="container">' +
                '<div class="row">' +
                '<div class="description col-md-6">' +
                '<div class="user row text-center">' +
                '<div class="user-img col-md-3">' +
                '<img src="' + item.userphoto + '" alt="User image">' +
                '</div>' +
                '<div class="user-info col-md-7 align-self-center">' +
                '<h4>' + item.user + '</h4>' +
                '<small>' + item.descriptionUser + '</small>' +
                '</div>' +
                '</div>' +
                '<div class="topics row">' +
                '<div class="col-md-12 text-center">' +
                '<h4>Topics</h4><div class="line-shape"></div>' +
                '<p>' + item.description + '</p>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="description text-center col-md-6">' +
                '<div class="col-md-12 text-center date">' +
                '<h4>Date</h4>' +
                '<p><span>Start: </span>' + item.start + '</p>' +
                '<p><span>End: </span>' + item.end + '</p>' +
                '</div>' +
                '<div class="clearfix"></div>' +
                '<div class="col-md-12 text-center pricing">' +
                '<h4>Pricing</h4>' +
                price +
                '<div class="col-md-6 offset-md-3">' +
                buttonAdd +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
    },
    getIn: function (item) {
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            headers: {"Authorization": localStorage['coins_token']},
            type: "POST",
            data: JSON.stringify({id:item.id}),
            url: UrlGetInChat,
            beforeSend: function (xhr) {
            },
            success: function (result) {
                var data = result.data;
                Chat.structureChat(item, $('#chats-container'));
                if (item.type == 'free') {
                	Chat.connectSession(item);
                }else{
                	localStorage['idChat'] = item.id;
                	localStorage['idService'] = 2;
                  window.location.href = UrlDashboardPay;
                }
            },
            error: function (xhr) {
              GeneratePop(xhr.responseJSON.message , "", "error" , "no", xhr);
        	    $("#addChat"+item.id).removeClass('btn-disabled').removeAttr('disabled').html('Buy Now!');
            },
            complete: function() {
              GeneratePop(xhr.responseJSON.message , "", "success" , "no");
            }
        });
    },
    filter: function (elem) {
        console.log($('#searchModal').find('.card.' + $(elem).attr('name')));
        if ($(elem).is(":checked")) {
            $('#searchModal').find('.card.' + $(elem).attr('name')).fadeIn('slow');
        } else {
            $('#searchModal').find('.card.' + $(elem).attr('name')).fadeOut();
        }
    }
}


$(document).ready(function () {
    HideLoader();
    Search.init();
    Chat.init();
});
