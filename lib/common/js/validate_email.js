var UrlValidateUser = BaseUrl + 'registro/valtkn';

function ValidateUserMail(id) {
    $.ajax({
        async: false,
        crossDomain: true,
        contentType: "application/json",
        dataType: "json",
        type: "POST",
        headers: {},
        data: JSON.stringify({tkn: id}),
        url: UrlValidateUser,
        beforeSend: function (xhr) {
        },
        success: function (result) {
            if (result.success == true) {
                $("#resp").html(_StructureValidate('text-success', '¡El código de seguridad enviado a su correo electronico es valido! y su usuario esta activo.', 'Para poder ingresar a nuestro portal pulse', '../Login'));
            } else {
                $("#resp").html(_StructureValidate('text-danger', '¡Este código de seguridad es invalido!', 'Para ir a la pantalla de inicio pulse', '../'));
            }
        },
        error: function (xhr) {
            GeneratePopUpAmigo("error","Error servicio token validación");
            console.log(xhr);
        },
        complete: function (jqXHR, textStatus) {
        }
    });
}
function _StructureValidate(TypeText, menssage, footer, url) {
    return  '<div class="text-center ' + TypeText + '">'
            + '<i class="fa fa-check-circle fa-5x" aria-hidden="true"></i>'
            + '<p class="mt-4">' + menssage + '</p>'
            + '</div>'
            + '<div class="form-group text-center mt-4">'
            + '<span>' + footer + ' <a href="' + url + '">Aqui</a>.</span>'
            + '</div>';
}
