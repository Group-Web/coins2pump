$(document).ready(function () {
    CreateChat.init();

});
var CreateChat = {
    init: function () {
        sessionStorage.removeItem('priceChat');
        localStorage.removeItem('idChat');
        this.requestListChats();
        $(".div-create").hide();
        // Eventos
        $('#startDateChat').datetimepicker({
            locale: 'ru',
            format: "YYYY-MM-DD HH:mm",
            minDate: new Date(),
            icons: {
                time: 'far fa-clock'
            }
        });
        $('#endDateChat').datetimepicker({
            locale: 'ru',
            format: "YYYY-MM-DD HH:mm",
            useCurrent: false,
            icons: {
                time: 'far fa-clock'
            }
        });
        $("#startDateChat").on("change.datetimepicker", function (e) {
            $('#endDateChat').datetimepicker('minDate', e.date);
        });
        $("#endDateChat").on("change.datetimepicker", function (e) {
            $('#startDateChat').datetimepicker('maxDate', e.date);
        });
        $('#createNewChat').on('click', function (e) {
            CreateChat.createNewChat();
        });
        $('#priceChatTable').on('click', '.delete', function (e) {
            CreateChat.deletePayment($(this).children('.delete').val());
        });
        $('#listChats').on('click', '.payment_chat', function (e) {
            localStorage["idChat"] = $(this).children('.payment_chat').val();
            localStorage["idService"] = 1;
            window.location.href = "../Dashboard/Payment/";
        });
        $("#add_payment_type").on("click", function () {
            CreateChat.addModalPayment();
        });
        $("#addChatNew").on("click", function () {
            CreateChat.saveNewChat();
        });
    },
    createNewChat: function () {
        this.requestCryptocurrency();
        $("#div-list").hide();
        $(".div-create").show("slow");
    },
    requestListChats: function () {
        var url = BaseUrl + 'chat/list';
        $.ajax({
            contentType: "application/json",
            dataType: "json",
            headers: {"Authorization": localStorage['coins_token']},
            type: "GET",
            url: url,
            beforeSend: function (xhr) {
                ShowLoader();
            },
            success: function (result) {
                $("#listChats").bootstrapTable("destroy");
                $('#listChats').bootstrapTable({
                    columns: [
                        {field: 'nameChat', title: 'Nombre'},
                        {field: 'nameCryptocurrency', title: 'Cryptocurrency'},
                        {field: 'startChat', title: 'Fecha inicio'},
                        {field: 'endChat', title: 'Fecha fin'},
                        {field: 'nameStatusChat', title: 'Estatus'},
                        {
                            field: 'idStatusChat',
                            title: 'Action',
                            align: 'center',
                            formatter: function (value, row) {
                                switch (value) {
                                    case 1:
                                        return '<a class="payment_chat" data-toggle="tooltip" title="Pagamento">' +
                                                '<input class="payment_chat" value="' + row.idChat + '" hidden/>' +
                                                '<i class="text-primary far fa-credit-card"></i></a>';
                                        break;
                                    case 2:
                                        return '<a class="" data-toggle="tooltip" title="Pagamento">' +
                                                '<i class="text-warning far fa-edit"></i></a>';
                                        break;
                                    case 3:
                                        return '<a class="" data-toggle="tooltip" title="Pagamento">' +
                                                '<i class="text-success far fa-check-square"></i></a>';
                                        break;
                                    case 4:
                                        return '<a class="" data-toggle="tooltip" title="Pagamento">' +
                                                '<i class="text-primary fas fa-calendar-check"></i></a>';
                                        break;
                                    case 5:
                                        return '<a class="" data-toggle="tooltip" title="Pagamento">' +
                                                '<i class="text-danger far fa-window-close"></i></a>';
                                        break;
                                    default:
                                        return '<a class="" data-toggle="tooltip" title="Pagamento">' +
                                                '<i class="text-primary fas fa-exclamation-circle"></i></a>';
                                        break;
                                }
                            }
                        }
                    ],
                    data: result.data,
                    pagination: true,
                    search: true,
                    showToggle: false
                });
            },
            error: function (xhr) {
                GeneratePop("!Crear Chat!", "", "error", "", xhr);
            },
            complete: function (jqXHR, textStatus) {
                HideLoader();
            }
        });
    },
    requestCryptocurrency: function () {
        var url = BaseUrl + 'cryptocurrency/';
        $.ajax({
            contentType: "application/json",
            dataType: "json",
            headers: {"Authorization": localStorage['coins_token']},
            type: "GET",
            url: url,
            beforeSend: function (xhr) {
                ShowLoader();
            },
            success: function (result) {
                $("#cryptocurrency").html('<option value = "0" hidden >Selecciona una cryptocurrency</option>');
                $.each(result.data, function (i, item) {
                    $("#cryptocurrency").append("<option value = '" + item.idCryptocurrency + "'>" + item.nameCryptocurrency + "</option>");
                });
                $("#cryptocurrency").prop('disabled', false);
            },
            error: function (xhr) {
                GeneratePop("!Crear Chat!", "", "error", "", xhr);
            },
            complete: function (jqXHR, textStatus) {
                HideLoader();
            }
        });
    },
    requestCryptocurrencyPayment: function () {
        var url = BaseUrl + 'payment/type';
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            headers: {"Authorization": localStorage['coins_token']},
            type: "GET",
            url: url,
            beforeSend: function (xhr) {
                ShowLoader();
            },
            success: function (result) {
                $("#cryptocurrency2").html('<option value = "0" hidden >Selecciona una cryptocurrency</option>');
                var array = [];
                $.each(result.data, function (i, item) {
                    if ($.inArray(item.idCryptocurrency, array) < 0) {
                        $("#cryptocurrency2").append("<option value = '" + item.idCryptocurrency + "'>" + item.nameCryptocurrency + "</option>");
                        array.push(item.idCryptocurrency);
                    }
                });
                $("#cryptocurrency2").prop('disabled', false);
            },
            error: function (xhr) {
                GeneratePop("Crear Chat", "", "error", "", xhr);
            },
            complete: function (jqXHR, textStatus) {
                HideLoader();
            }
        });
    },
    addModalPayment: function () {
        var that = this;
        that.requestCryptocurrencyPayment();
        swal({
            title: "<h4>¡Agrega un nuevo metodo de pago!</h4>",
            text: this.structureModalPayment(),
            type: 'info',
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Guardar",
            cancelButtonText: "Cancelar",
            cancelButtonClass: "btn-default",
            allowOutsideClick: true,
            html: true,
            closeOnConfirm: true
        }, function () {
            that.addPayment($("#valuePriceChat").val(), $("#descriptionPriceChat").val(), $("#cryptocurrency2 option:selected").val(), $("#cryptocurrency2 option:selected").text());
            that.paintPayment();
        });
    },
    structureModalPayment: function () {
        return '<div class="text-left" style="padding:10px 0;"><label>Monto:</label><div class="input-group">' +
                '<input placeholder="Ej: 0.01" type="text" class="form-control" id="valuePriceChat">' +
                '</div>' +
                '<input type="hidden" id="descriptionPriceChat" value=" ">' +
//                '</br><div class="input-group"><input placeholder="Descripcion" type="text" class="form-control" id="descriptionPriceChat"></div>' +
                '</br><label>Cryptocurrency:</label><div class="input-group">' +
                '<select class="form-control input-new-c cryptocurrency" id="cryptocurrency2" ></select>' +
                '</div></div>';
    },
    addPayment: function (valuePriceChat, descriptionPriceChat, cryptocurrency, nameCryptocurrency) {
        if (sessionStorage['priceChat']) {
            var array = JSON.parse(sessionStorage['priceChat']);
            var data = array.data;
            var msj = "";
            for (var i in data) {
                if (cryptocurrency == data[i].cryptocurrency) {
                    msj = '<small>- Ya existe un monto con esta criptomoneda</small>';
                }
                if (msj != '') {
                    $(".rs").html(msj);
                    return false;
                }
            }
            array.data.push({
                valuePriceChat: valuePriceChat,
                descriptionPriceChat: descriptionPriceChat,
                cryptocurrency: cryptocurrency,
                nameCryptocurrency: nameCryptocurrency
            });
            sessionStorage['priceChat'] = JSON.stringify({data: array.data});
        } else {
            sessionStorage['priceChat'] = JSON.stringify({data: [{valuePriceChat: valuePriceChat, descriptionPriceChat: descriptionPriceChat, cryptocurrency: cryptocurrency, nameCryptocurrency: nameCryptocurrency}]});
        }
    },
    paintPayment: function () {
        if (sessionStorage['priceChat']) {
            var data = JSON.parse(sessionStorage['priceChat']).data;
            $("#priceChatTable").show();
            $("#priceChatTableLegend").hide();
            $("#priceChatTable").bootstrapTable("destroy");
            $('#priceChatTable').bootstrapTable({
                columns: [
                    {field: 'nameCryptocurrency', title: 'Cryptocurrency'},
                    {field: 'valuePriceChat', title: 'Monto'},
//                    {field: 'descriptionPriceChat', title: 'Descripcion'},
                    {
                        field: 'cryptocurrency',
                        title: 'Action',
                        align: 'center',
                        formatter: function (value, row) {
                            return '<a class="delete" data-toggle="tooltip" title="Delete">' +
                                    '<input class="delete" value="' + value + '" hidden/>' +
                                    '<i class="text-danger fas fa-window-close"></i></a>';
                        }
                    }
                ],
                data: data,
                pagination: false,
                search: false,
                showToggle: false
            });
        } else {
            $("#priceChatTable").hide();
            $("#priceChatTableLegend").show();
            $("#priceChatTableLegend").html("<p>Para agregar un precio al chat presione el boton +, de lo contrario sera tomado como un chat gratuito</p>");
        }

    },
    deletePayment: function (cryptocurrency) {
        if (sessionStorage['priceChat']) {
            var data = JSON.parse(sessionStorage['priceChat']).data;
            for (var i in data) {
                if (cryptocurrency == data[i].cryptocurrency) {
                    data.splice(i, 1);
                }
            }
            if (data != '') {
                sessionStorage['priceChat'] = JSON.stringify({data: data});
            } else {
                sessionStorage.removeItem('priceChat');
            }
            this.paintPayment();
        }
    },
    saveNewChat: function () {
        var url = BaseUrl + 'chat/';
        var price = JSON.parse(sessionStorage['priceChat']);
        $.ajax({
            contentType: "application/json",
            dataType: "json",
            type: "POST",
            headers: {"Authorization": localStorage['coins_token']},
            data: JSON.stringify({
                nameChat: $("#nameChat").val(),
                startDateChat: $("#startDateChat").val(),
                endDateChat: $("#endDateChat").val(),
                descriptionChat: $("#descriptionChat").val(),
                cryptocurrency: $("#cryptocurrency").val(),
                priceChat: price.data
            }),
            url: url,
            beforeSend: function (xhr) {
                ShowLoader();
            },
            success: function (result) {
                GeneratePop("Crear Chat", result.message, "success", "");
            },
            error: function (xhr) {
                GeneratePop("Crear Chat", "", "error", "no", xhr);
            },
            complete: function (jqXHR, textStatus) {
                HideLoader();
            }
        });
    }
};
