$(document).ready(function () {
    Login.init();
});
var Login = {
    init: function () {
        HideLoader();
        var that = this;
        ValidateOnKeyUpLogin();
        $('#section-login').keypress(function (event) {
            if (event.which == 13) {
                that.getLoginUser();
            }
        });
        $('#sendLogin').click(function (e) {
            that.getLoginUser();
        });
    },
    getLoginUser: function () {
        if (!ValidateLogin()) {
            return false;
        }
        var url = BaseUrl + 'user/login/';
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "POST",
            url: url,
            data: JSON.stringify({loginUser: $("#loginUser").val(), passwordUser: $("#passwordUser").val()}),
            beforeSend: function (xhr) {
                ShowLoader();
            },
            success: function (result) {
                // Asignar valores
                var decoded = jwt_decode(result.data.token);
                localStorage['coins_token'] = result.data.token;
                localStorage['coins_login'] = decoded.data.loginUser;
                localStorage['coins_name'] = decoded.data.nameUser;
                localStorage['coins_lastname'] = decoded.data.lastnameUser;
                // Mensaje de bienvenida
                swal({
                    title: "<span class='text-success'> ¡Bienvenido, " + localStorage['coins_login'] + "! </span>",
                    text: " Plataforma Coins2pump",
                    imageUrl: '../lib/common/img/coins2pump-logo.png',
                    imageSize: '150x80',
                    imageAlt: 'Custom image',
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Entrar",
                    closeOnConfirm: false,
                    allowOutsideClick: false,
                    html: true
                }, function () {
                    window.location.href = "../Dashboard";
                });
            }
            ,
            error: function (xhr) {
                $(".icon-login").html("<span class='text-danger fas fa-user fa-4x'></span>");
                GeneratePop("!Iniciar Session!", "", "error", "no", xhr);
                ValidCustom(xhr.responseJSON.validation, "Login");
            },
            complete: function () {
                HideLoader();
            }
        });
    }
};