var UrlServiceCountry = BaseUrl + 'default/countries';
var UrlServiceGender = BaseUrl + 'default/genders';
var UrlServiceAddUser = BaseUrl + 'user/';
var UrlServiceResendMail = BaseUrl + 'user/resendmail';

// Auto Load
function LoadRegister() {
    $(document).ajaxComplete(function () {
        HideLoader();
    });
    GetCountries("#countryUser");
    GetGenders("#genderUser");
    ValidateOnKeyUpRegister();
    $(".div-register-mail").hide();
    $("#addUser").on("click", function () {
        if (ValidateRegister()) {
            AddUser();
        }
    });
    $("#resend_email").on("click", function () {
        ResendEmail($("#emailUser").val());
    });
}

// Cargar ComboBox
function GetCountries(etiqueta) {
    $.ajax({
        async: true,
        crossDomain: true,
        contentType: "application/json",
        dataType: "json",
        headers: {},
        type: "GET",
        url: UrlServiceCountry,
        beforeSend: function (xhr) {
            $(etiqueta).prop('disabled', true);
        },
        success: function (result) {
            $(etiqueta).html('<option value = "0" hidden >Selecciona un País</option>');
            $.each(result.data, function (i, item) {
                $(etiqueta).append("<option value = '" + item.idCountry + "'>" + item.nameCountry + "</option>");
            });
            $(etiqueta).prop('disabled', false);
        },
        error: function (xhr) {
            GeneratePop("!Error paises!", "", "error", "no", xhr);
//            GeneratePopUpAmigo("error", "Error cargando los paises");
//            console.log(xhr);
        },
        complete: function (jqXHR, textStatus) {
        }
    });
}
function GetGenders(etiqueta) {
    $.ajax({
        async: true,
        crossDomain: true,
        contentType: "application/json",
        dataType: "json",
        headers: {},
        type: "GET",
        url: UrlServiceGender,
        beforeSend: function (xhr) {
            $(etiqueta).prop('disabled', true);
        },
        success: function (result) {
            $(etiqueta).html('<option value = "0" hidden >Selecciona un Genero</option>');
            $.each(result.data, function (i, item) {
                $(etiqueta).append("<option value = '" + item.idGender + "'>" + item.nameGender + "</option>");
            });
            $(etiqueta).prop('disabled', false);
        },
        error: function (xhr) {
            GeneratePop("!Error generos!", "", "error", "no", xhr);
        },
        complete: function (jqXHR, textStatus) {
        }
    });
}

// Insertar Usuario
function AddUser() {
    $.ajax({
        async: false,
        crossDomain: true,
        contentType: "application/json",
        dataType: "json",
        type: "POST",
//        headers: {"Authorization": localStorage['coins_token']},
        data: JSON.stringify({
            nameUser: $("#nameUser").val(),
            lastnameUser: $("#lastnameUser").val(),
            birthdayUser: $("#birthdayUser").val(),
            fkGenderUser: $("#genderUser").val(),
            fkCountryUser: $("#countryUser").val(),
            addressUser: " ",
            loginUser: $("#loginUser").val(),
            emailUser: $("#emailUser").val(),
            celUser: " ",
            phoneUser: " ",
            passwordUser: $("#passwordUser").val()
        }),
        url: UrlServiceAddUser,
        beforeSend: function (xhr) {},
        success: function (result) {
            $(".div-register").hide();
            $(".div-register-mail").show("slow");
            GeneratePop("!Registrar Usuario!", result.message, "success", "no");
        },
        error: function (xhr, result) {
            GeneratePop("!Registrar Usuario!", "", "error", "no", xhr);
            ValidCustom(xhr.responseJSON.validation, "Register");
        },
        complete: function (jqXHR, textStatus) {
        }
    });
}

// Reenviar Correo
function ResendEmail(email) {
    $.ajax({
        async: true,
        crossDomain: true,
        contentType: "application/json",
        dataType: "json",
        type: "POST",
        headers: {},
        data: JSON.stringify({mail: email}),
        url: UrlServiceResendMail,
        beforeSend: function (xhr) {
        },
        success: function (result) {
            GeneratePop("!Registrar Usuario!", result.message, "success", "no");
        },
        error: function (xhr) {
            GeneratePop("!Reenviando el correo!", "", "error", "no", xhr);
        },
        complete: function (jqXHR, textStatus) {
        }
    });
}