$(document).ready(function () {
    Payment.init();
    $("#header_tabs").on("click", ".nav-link", function () {
        Payment.generatePayment($(this).data("value"));
    });
    $("#comprar").on("click", function () {
        Payment.addPayment();
    });
});

var Payment = {
    init: function () {
        if (!localStorage['idService']) {
            GeneratePop("¡Pagamento!", "Debe seleccionar que pagar", "error", HomeUrl + "Dashboard");
        }
        this.requestListChats();
    },
    requestListChats: function () {
        var that = this;
        var url = BaseUrl + 'payment/listByChat';
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "POST",
            headers: {"Authorization": localStorage['coins_token']},
            data: JSON.stringify({
                idChat: parseInt(localStorage['idChat']),
                idServicePayment: parseInt(localStorage['idService'])
            }),
            url: url,
            beforeSend: function (xhr) {
                ShowLoader();
            },
            success: function (result) {
                sessionStorage['priceChat'] = JSON.stringify({data: result.data['priceChat']});
                that.infoPaymentView(result.data);
                that.comboPayment(result.data['priceChat']);
            },
            error: function (xhr) {
                GeneratePop("¡Pagamento!", "", "error", HomeUrl + "Dashboard", xhr);
            },
            complete: function (jqXHR, textStatus) {
                HideLoader();
            }
        });
    },
    infoPaymentView: function (data) {
        var html = '<div class="text-left"><p><label>Tipo de pago: </label> ' + data.nameServicePayment + '</p>';
        if (parseInt(data.idServicePayment) > 2) {
            data.nameChat = "N/A";
        }
//            html += '<p><label>Numero REF:</label> ' + data.idChat + '</p>';
        html += '<p><label>Nombre del Chat:</label> ' + data.nameChat + '</p>';
//            html += '<p><label>Descr:</label> ' + data.descriptionChat + '</p>';
        html += '</div>';
        $("#infoPayment").html(html);
    },
    comboPayment: function (data) {
        $("#header_tabs").html(" ");
        var active = "";
        $.each(data, function (i, item) {
            if (i == 0) {
                active = "active";
                Payment.generatePayment(item.idCryptocurrency);
            } else {
                active = " ";
            }
            $("#header_tabs").append('<li class="nav-item"><a class="nav-link ' + active + '" data-value="' + item.idCryptocurrency + '" role="tab" data-toggle="tab"><img src="' + item.imageCryptocurrency + '" alt="' + item.codeCryptocurrency + '"></a></li>');
        });
    },
    generatePayment: function (val) {
        $("#qrcode").empty();
        var data = JSON.parse(sessionStorage['priceChat']).data;
        for (var i in data) {
            if (val == data[i].idCryptocurrency) {
                $("#qrcode").qrcode({text: data[i].uriCryptocurrency.toString()});
                $("#strcode").html(data[i].uriCryptocurrency.toString());
                $("#cryptocurrency_img").attr("src", data[i].imageCryptocurrency);
                $("#valuePayment").html(data[i].valuePayment + " " + data[i].codeCryptocurrency);
                $(".div-va").show();
                break;
            }
        }
    },
    addPayment: function () {
        var that = this;
        var typePayment = $(".nav-link.active").data("value");
        var url = BaseUrl + 'payment/new';
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            type: "POST",
            headers: {"Authorization": localStorage['coins_token']},
            data: JSON.stringify({
                ammountPayment: $("#valuePayment").val(),
                referencePayment: $("#referencePayment").val(),
                descriptionPayment: "prueba front",
                idChat: parseInt(localStorage['idChat']),
                typePayment: typePayment,
                fkServicePayment: parseInt(localStorage["idService"])
//                            fkCryptocurrency: parseInt(),
            }),
            url: url,
            beforeSend: function (xhr) {
                ShowLoader();
            },
            success: function (result) {
                if (parseInt(localStorage["idService"]) == 1) {
                    GeneratePop("¡Pagamento!", result.message, "success", "../../Chat/create.php");
                } else if (parseInt(localStorage["idService"]) == 2) {
                    GeneratePop("¡Pagamento!", result.message, "success", "../../Chat/");
                } else {
                    GeneratePop("¡Pagamento!", result.message, "success", "../../Coupon/");
                }
            },
            error: function (xhr) {
                GeneratePop("¡Pagamento!", "", "error", "no", xhr);
            },
            complete: function (jqXHR, textStatus) {
                HideLoader();
            }
        });
    }
};