var UrlUserEdit = BaseUrl + 'user/';
var UrlVideo = BaseUrl + 'user/video/';
var UrlServiceCountry = BaseUrl + 'default/countries';
var UrlLastVideo = BaseUrl + 'default/video_list';

var Profile = {
    init: function () {
    },
    request: function () {
        var that = this;
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            headers: {"Authorization": localStorage['coins_token']}, //localStorage['coins_token']
            type: "GET",
            url: UrlProfile,
            beforeSend: function (xhr) {
            },
            success: function (result) {
                that.completeFields(result);
                console.log(result);
                $.each(result.data.videos, function (i, item) {
                    that.videoDetail(item);
                });


                if (userProfile == '') {
                    $('#profile-info').append('<button  class="btn btn-theme" id="openEdit">Edit my Profile</button>');
                    var structure = '<div class="row">' +
                            '<div class="col-md-8 offset-md-2 text-center">' +
                            '<div class="form-group" id="addVideoForm" style="display:none;">' +
                            '<input type="text" class="form-control" id="idvideo" placeholder="Id Video"></input>' +
                            '<small style="color: #a7a7a7;">https://www.youtube.com/watch?v=<strong style="color:#02ea02;">XYv3BSCjeJU</strong></small>' +
                            '<hr><button class="btn btn-theme" id="sendVideoAdd">Add</button>' +
                            '</div>' +
                            '<button class="btn btn-theme" id="addVideo"><i class="fas fa-plus"></i> Add Video</button>' +
                            '</div>' +
                            '</div>';

                    $('#info-container').append(structure);
                }

                $('#info-container').on('click', '#addVideo', function (event) {
                    event.preventDefault();
                    $('#addVideo').fadeOut('slow', function () {
                        $('#addVideoForm').fadeIn('slow', function () {});
                    });
                });

                $('#info-container').on('click', '#sendVideoAdd', function (event) {
                    that.addVideo($('input#idvideo').val());
                });

                $('.video-container').on('click', '.delete-icon', function (event) {
                    event.preventDefault();
                    var id = $(this).attr('id');
                    that.deleteVideo(id);
                });

            },
            error: function (xhr) {
                GeneratePop(xhr.responseJSON.message, "", "error", "no", xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    },
    videoDetail: function (id) {

        // API KEY AIzaSyCY_k3QiP7rUoMecA8U3Z0Iugyybaxda0A
        var youTubeURL = 'https://www.googleapis.com/youtube/v3/videos';
        $.ajax({
            async: false,
            global: false,
            url: youTubeURL,
            dataType: "json",
            method: 'GET',
            data: {
                key: 'AIzaSyCY_k3QiP7rUoMecA8U3Z0Iugyybaxda0A',
                id: id,
                part: 'snippet',
            },
            'success': function (data) {
                if ($('iframe#ytiframe').length == 0) {
                    var structure = '<div class="row" id="video-embed">' +
                            '<iframe id="ytiframe" width="700" height="400" src="https://www.youtube.com/embed/' + id + '"></iframe><div class="pl-3 pr-3 pt-3 pb-3">' +
                            '<h3>' + data.items[0].snippet.localized.title + '</h3>' +
                            '<span>' + data.items[0].snippet.localized.description.substring(0, 300) + ' . . . </span></div>' +
                            '</div><hr>';
                    $('#info-container').append(structure);
                }

                var itemDelete = '';
                if (userProfile == '') {
                    itemDelete = '<i class="fas fa-trash-alt delete-icon" id="' + id + '"></i>';
                }


                var structure = '<div class="row video-container" id="' + id + '">' +
                        itemDelete +
                        '<div class="col-md-3 thumb">' +
                        '<img id="' + id + '" src="https://i.ytimg.com/vi/' + id + '/mqdefault.jpg" alt="">' +
                        '</div>' +
                        '<div class="col-md-9 info">' +
                        '<p class="text-center" id="' + id + '">' + data.items[0].snippet.localized.title + '</p>' +
                        '<span>' + data.items[0].snippet.localized.description.substring(0, 150) + ' . . . </span>' +
                        '</div>' +
                        '</div>';
                $('#info-container').append(structure);


                $('.video-container').on('click', '#' + id, function (event) {
                    event.preventDefault();
                    var structure = '<iframe id="ytiframe" width="700" height="400" src="https://www.youtube.com/embed/' + id + '"></iframe>' +
                            '<div class="pl-3 pr-3 pt-3 pb-3">' +
                            '<h3>' + data.items[0].snippet.localized.title + '</h3>' +
                            '<span>' + data.items[0].snippet.localized.description.substring(0, 300) + ' . . . </span>' +
                            '</div>';
                    $('#video-embed').html('');
                    $('#video-embed').append(structure);
                });
            }
        });
    },
    completeFields: function (result) {
        var that = this;
        if (result.data.nameUser != '') {
            $('#name').html(result.data.nameUser + ' ' + result.data.lastnameUser);
            $('input#name').val(result.data.nameUser);
            $('input#lastname').val(result.data.lastnameUser);
        }
        if (result.data.loginUser != '') {
            $('#username').html('<i class="fas fa-user-circle"></i> &nbsp; ' + result.data.loginUser);
            $('input#username').val(result.data.loginUser);
        }
        if (result.data.emailUser != '') {
            $('#email').html('<i class="fas fa-at"></i> &nbsp;' + result.data.emailUser);
            $('input#email').val(result.data.emailUser);
        }
        if (result.data.nameCountryUser != '') {
            GetCountries("select#country", result.data.idCountryUser);
            $('#country').html(result.data.nameCountryUser);
        }
        if (result.data.descriptionUser != '') {
            $('#description').html(result.data.descriptionUser);
            $('textarea#description').html(result.data.descriptionUser);
        }

        $('.profile').on('click', '#openEdit', function (event) {
            event.preventDefault();
            $('#profile-info').fadeOut('slow', function () {
                $('.profile-form').fadeIn('slow', function () {});
            });
        });
        $('#sendEdit').on('click', function (event) {
            that.updateData();
        });
    },
    updateData: function () {
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                name: $("input#name").val(),
                lastname: $("input#lastname").val(),
                //username: $("input#username").val(),
                email: $("input#email").val(),
                country: $("select#country").val(),
                description: $("textarea#description").val()
            }),
            headers: {"Authorization": localStorage['coins_token']}, //localStorage['coins_token']
            type: "PUT",
            url: UrlUserEdit,
            beforeSend: function (xhr) {
            },
            success: function (result) {
                GeneratePop(result.message, "", "success", "no");
                $('.profile-form').fadeOut('slow', function () {
                    $('#profile-info').fadeIn('slow');
                });
            },
            error: function (xhr) {
                GeneratePop(xhr.responseJSON.message, "", "error", "no", xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    },
    addVideo: function (id) {
        var that = this;
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                id: id
            }),
            headers: {"Authorization": localStorage['coins_token']}, //localStorage['coins_token']
            type: "POST",
            url: UrlVideo,
            beforeSend: function (xhr) {
            },
            success: function (result) {
                location.reload();
            },
            error: function (xhr) {
                GeneratePop(xhr.responseJSON.message, "", "error", "no", xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    },
    deleteVideo: function (id) {
        var that = this;
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                id: id
            }),
            headers: {"Authorization": localStorage['coins_token']}, //localStorage['coins_token']
            type: "DELETE",
            url: UrlVideo,
            beforeSend: function (xhr) {
            },
            success: function (result) {
                location.reload();
            },
            error: function (xhr) {
                GeneratePop(xhr.responseJSON.message, "", "error", "no", xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    }
};

var ViewVideos = {
    init: function () {
    },
    request: function () {
        var that = this;
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            headers: {"Authorization": localStorage['coins_token']}, //localStorage['coins_token']
            type: "GET",
            url: UrlLastVideo,
            beforeSend: function (xhr) {
            },
            success: function (result) {
                $('#info-container2').html("");
                $.each(result.data, function (i, item) {
                    that.videoDetail(item);
                });
            },
            error: function (xhr) {
                GeneratePop(xhr.responseJSON.message, "", "error", "no", xhr);
            },
            complete: function (jqXHR, textStatus) {
                HideLoader();
            }
        });
    },
    videoDetail: function (val) {
        var youTubeURL = 'https://www.googleapis.com/youtube/v3/videos';
        $.ajax({
            async: false,
            global: false,
            url: youTubeURL,
            dataType: "json",
            method: 'GET',
            data: {
                key: 'AIzaSyCY_k3QiP7rUoMecA8U3Z0Iugyybaxda0A',
                id: val.code,
                part: 'snippet',
            },
            'success': function (data) {
                if ($('iframe#ytiframe').length == 0) {
                    var structure = '<div class="col-md-12" id="video-embed">' +
                            '<iframe id="ytiframe" width="700" height="500" src="https://www.youtube.com/embed/' + val.code + '"></iframe><div class="pl-3 pr-3 pt-3 pb-3">' +
                            '<h3>' + data.items[0].snippet.localized.title + '</h3>' +
                            '<span>' + data.items[0].snippet.localized.description.substring(0, 300) + ' . . . </span></div>' +
                            '</div><hr>';
                    $('#info-container2').append(structure);
                }

                var structure2 = '<div class="col-12 col-md-3 video-container2" id="' + val.code + '"><div class="video-wrap">' +
                        '<div class="text-success "><a href="../../Profile/?user=' + val.loginUser + '"><i class="fas fa-user"></i> <b>' + val.loginUser + '</b></a></div>' +
                        '<div class="thumb text-center">' +
                        '<img id="' + val.code + '" src="https://i.ytimg.com/vi/' + val.code + '/mqdefault.jpg" alt="">' +
                        '</div>' +
                        '<p class="" id="' + val.code + '">' + data.items[0].snippet.localized.title.substring(0, 60) + ' . . .  </p>' +
                        '</div></div>';
                $('#info-container2').append(structure2);


                $('.video-container2').on('click', '#' + val.code, function (event) {
                    event.preventDefault();
                    var structure = '<iframe id="ytiframe" width="700" height="500" src="https://www.youtube.com/embed/' + val.code + '"></iframe>' +
                            '<div class="pl-3 pr-3 pt-3 pb-3">' +
                            '<h3>' + data.items[0].snippet.localized.title + '</h3>' +
                            '<span>' + data.items[0].snippet.localized.description.substring(0, 300) + ' . . . </span>' +
                            '</div>';
                    $('#video-embed').html('');
                    ShowLoader();
                    $('#video-embed').append(structure);
                    HideLoader();
                });
            }
        });
    },
    updateData: function () {
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                name: $("input#name").val(),
                lastname: $("input#lastname").val(),
                //username: $("input#username").val(),
                email: $("input#email").val(),
                country: $("select#country").val(),
                description: $("textarea#description").val()
            }),
            headers: {"Authorization": localStorage['coins_token']}, //localStorage['coins_token']
            type: "PUT",
            url: UrlUserEdit,
            beforeSend: function (xhr) {
            },
            success: function (result) {

            },
            error: function (xhr) {
                GeneratePop(xhr.responseJSON.message, "", "error", "no", xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    },
    addVideo: function (id) {
        var that = this;
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                id: id
            }),
            headers: {"Authorization": localStorage['coins_token']}, //localStorage['coins_token']
            type: "POST",
            url: UrlVideo,
            beforeSend: function (xhr) {
            },
            success: function (result) {
                location.reload();
            },
            error: function (xhr) {
                GeneratePop(xhr.responseJSON.message, "", "error", "no", xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    },
    deleteVideo: function (id) {
        var that = this;
        $.ajax({
            async: true,
            crossDomain: true,
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                id: id
            }),
            headers: {"Authorization": localStorage['coins_token']}, //localStorage['coins_token']
            type: "DELETE",
            url: UrlVideo,
            beforeSend: function (xhr) {
            },
            success: function (result) {
                location.reload();
            },
            error: function (xhr) {
                GeneratePop(xhr.responseJSON.message, "", "error", "no", xhr);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    }
};

// Cargar ComboBox
function GetCountries(etiqueta, selected) {
    $.ajax({
        async: true,
        crossDomain: true,
        contentType: "application/json",
        dataType: "json",
        headers: {},
        type: "GET",
        url: UrlServiceCountry,
        beforeSend: function (xhr) {
            $(etiqueta).prop('disabled', true);
        },
        success: function (result) {
            $(etiqueta).html('<option value = "0" hidden >Selecciona un País</option>');
            $.each(result.data, function (i, item) {
                var selectedTag = "";
                if (parseInt(item.idCountry) == parseInt(selected)) {
                    selectedTag = "selected";
                }
                $(etiqueta).append("<option " + selectedTag + " value = '" + item.idCountry + "'>" + item.nameCountry + "</option>");
            });
            $(etiqueta).prop('disabled', false);
        },
        error: function (xhr) {
            GeneratePop(xhr.responseJSON.message, "", "error", "no", xhr);
        },
        complete: function (jqXHR, textStatus) {
        }
    });
}
