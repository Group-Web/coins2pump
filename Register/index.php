<?php
$url = "../";
$name = "Register";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html lang="pt">

    <head>
        <?php head($url, $name); ?>
    </head>

    <body>
        <?php loader(); ?>

        <?php menu($url, $name); ?>

        <!-- TITLE SECTION -->
        <section class="page-header clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1>Cadastra-se</h1> 
                        <div class="line-shape line-shape-center"></div> 
                    </div>
                </div>
            </div>
        </section>
        <!-- / TITLE SECTION -->

        <section class="special-area bg-white section_padding_50" id="section-register">

            <div class="div-register">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text" class="form-control input-new" id="nameUser" placeholder="Nome" required="" autocomplete="off">
                                <div class="div-validador">
                                    <div>&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>lastnameUser</label>
                                <input type="text" class="form-control input-new" id="lastnameUser" placeholder="lastnameUser" required="" autocomplete="off">
                                <div class="div-validador">
                                    <div>&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <label>Usuário</label>
                            <div class="form-group">
                                <input type="text" class="form-control input-new" id="loginUser" placeholder="Usuário" required="" autocomplete="off">
                                <div class="div-validador">
                                    <div>&nbsp;</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <label>E-mail</label>
                            <div class="form-group">
                                <input type="email" class="form-control input-new" id="emailUser" placeholder="E-mail" required="" autocomplete="off">
                                <div class="div-validador">
                                    <div>&nbsp;</div>
                                </div>
                            </div>
                        </div>  
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Genero</label>
                                <select class="form-control input-new-c" id="genderUser" ></select>
                                <div class="div-validador">
                                    <div>&nbsp;</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Cumpleaños</label>
                                <input class="form-control input-new" type="date" id="birthdayUser">
                                <div class="div-validador">
                                    <div>&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Pais</label>
                                <select class="form-control input-new-c" id="countryUser" ></select>
                                <div class="div-validador">
                                    <div>&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <!--                        <div class="col-md-5">
                                                    <label>Direccion</label>
                                                    <div class="form-group">
                                                        <textarea type="text" class="form-control input-new" id="addressUser" required=""></textarea>
                                                        <div class="div-validador">
                                                            <div>&nbsp;</div>
                                                        </div>
                                                    </div>
                                                </div>-->

                        <!--                        <div class="col-md-5">
                                                    <label>Telefone movil</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-new" id="celUser" placeholder="Telefone movil" required="" autocomplete="off">
                                                        <div class="div-validador">
                                                            <div>&nbsp;</div>
                                                        </div>
                                                    </div>
                                                </div>-->
                        <!--                        <div class="col-md-5">
                                                    <label>Telefone</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-new" id="phoneUser" placeholder="Telefone" required="" autocomplete="off">
                                                        <div class="div-validador">
                                                            <div>&nbsp;</div>
                                                        </div>
                                                    </div>
                                                </div>-->
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Senha</label>
                                <input type="password" class="form-control input-new" id="passwordUser" placeholder="Senha" required="" autocomplete="off">
                                <div class="div-validador">
                                    <div>&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Confirmar Senha</label>
                                <input type="password" class="form-control input-new" id="confirmPasswordUser" placeholder="Confirmar Senha" required>
                                <div class="div-validador">
                                    <div>&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <button id="addUser" type="submit" class="btn submit-btn">Cadastrar</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="div-register-mail">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-5">
                            <h3 class="text-success">¡Usuario Registrado Correctamente!</h3>
                            <div class="text-center mt-4 mb-4">
                                <i class="fa fa-envelope fa-4x" aria-hidden="true"></i>
                                <p class="mt-4">Se ha enviado un e-mail a su dirección de correo electrónico. 
                                    Compruebe su correo y valide su cuenta. Este proceso puede tardar algunos minutos. 
                                    Verifique tambien dentro de su carpeta de spam.</p>
                            </div>    
                            <div class="form-group text-center mt-4">
                                <span>Si no ha recibido el correo pulse <a id="resend_email" href="#">Aqui</a>.</span>
                                <br>
                                <span>Si ya lo recibio <a id="resend_email" href="../Login/">Inicie Session</a>.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <?php include '../promotion.php'; ?>
    <?php footer($url); ?>
    <?php scripts($url); ?>
    <script src="../lib/common/js/validate.js"></script>
    <script src="../lib/common/js/register.js"></script>
    <script>
        $(document).ready(function () {
            LoadRegister();
        });
    </script>
</body>

</html>