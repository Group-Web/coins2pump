<?php
$url = "./";
$name = "Home";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html lang="pt">

    <head>
        <?php head($url, $name); ?>
    </head>

    <body>
        <?php loader(); ?>

        <?php menu($url, $name); ?>

        <!-- ***** Wellcome Area Start ***** -->
        <section class="wellcome_area clearfix" id="home">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 col-md">
                        <div class="wellcome-heading text-center">
                            <h2>Conectamos pessoas para multiplicar Bitcoins</h2>
                            <h3>C</h3>
                            <p class="subtitle">Consiga lucro rápido todos os dias disputando ordens de compra e venda de criptomoedas</p>
                        </div>
                        <div class="get-start-area col-md-12 mt-30" style="display: inline-block;">
                            <!-- Form Start -->
                            <!--<form action="#" method="post" class="form-inline" style="width: 100%; display: inline-block;">-->
                                <!--<input type="email" class="form-control email" placeholder="name@company.com">-->
                            <input id="btns" type="submit" class="submit" value="Cadastrase">
                            <!--</form>-->
                            <!-- Form End -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Welcome thumb -->
            <div class="welcome-thumb wow fadeInDown" data-wow-delay="0.5s">
                <!--<img src="img/bg-img/welcome-img.png" alt="">-->
            </div>
        </section>
        <!-- ***** Wellcome Area End ***** -->

        <!-- ***** Special Area Start ***** -->
        <section class="special-area bg-white section_padding_100" id="quienessomos">
            <div class="container custom-container">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 align-self-center" style="padding: 20px 0; ">
                            <img id="img-comofunciona" src="<?php echo $url; ?>lib/common/img/coins2pump-section-2.png" alt="">
                        </div>
                        <div class="col-md-6 align-self-center a">
                            <h4>Juntos somos mais fortes!</h4>
                            <br><br>
                            <p>A Coins2pump é uma plataforma de conteúdo e
                                entretenimento para investidores de criptomoedas.</p>
                            <p>O algoritmo do chat identifica as moedas virtuais
                                com potencial de valorização para você lucrar todos
                                os dias.</p>
                            <p>O nosso propósito é conectar pessoas para
                                multiplicar bitcoins, através de conteúdo, sinais,
                                games, pump, não importa, este é o nosso objetivo.</p>
                            <p>Acreditamos que criptomoeda é melhor que
                                dinheiro e combina com a nossa visão de mundo e
                                liberdade.</p>
                            <p>Você se identifica com a gente? Bem-vindo!</p>
                            <br>
                            <span><strong>Somos Coins2pump</strong></span><br>
                            <span><strong>ousados por natureza, rebeldes por opção.</strong></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Special Area End ***** -->

    <!-- ***** Custom Area Start ***** -->
    <section class="special-area bg-white" id="random-img">
        <img src="<?php echo $url; ?>lib/common/img/coins2pump-people.jpg" alt="">
    </section>
    <!-- ***** Custom Area End ***** -->

    <!-- ***** Special Area Start ***** -->
    <section class="special-area bg-white section_padding_100" id="comofunciona">
        <div class="container custom-container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading Area -->
                    <div class="section-heading text-center">
                        <h2>¿Como funciona?</h2>
                        <div class="line-shape"></div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-5 como-f">
                        <img id="img-comofunciona" src="<?php echo $url; ?>lib/common/img/coins2pump-funciona.png" alt="">
                    </div>
                    <div class="col-md-6 offset-md-1 col align-self-center">
                        <p>Quer ensinar? Divulgue o seu conteúdo do
                            youtube e participe do chat. Os campeões de
                            audiência ganham bônus em criptomoedas</p>
                        <br>
                        <p>Quer aprender? Acesse ao conteúdo feito
                            pela comunidade</p>
                        <br>
                        <p>Quer trocar idéia? Participe do chat e
                            comece a interagir com outros
                            investidores</p>
                        <br>
                        <p>Pump? Identificamos as moedas virtuais com
                            potencial de valorização. Reúna a galera e
                            dispute o lucro rápido todos os dias</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
  
    <!-- ***** CTA Area Start ***** -->
    <section class="our-monthly-membership section_padding_50 clearfix" id="payments">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading text-center">
                        <h2>Pague com</h2>
                        <div class="line-shape"></div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="item-content col-md-12 text-center">
                                <img src="<?php echo $url; ?>lib/common/img/coins2pump-btc.png" alt="">
                                <p>BITCOIN</p>
                                <!--<p>0.0035</p>-->
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-content col-md-12 text-center">
                                <img src="<?php echo $url; ?>lib/common/img/coins2pump-ethereum.png" alt="">
                                <p>ETHEREUM</p>
                                <!--<p>0.0400</p>-->
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-content col-md-12 text-center">
                                <img src="<?php echo $url; ?>lib/common/img/coins2pump-ripple.png" alt="">
                                <p>RIPPLE</p>
                                <!--<p>36.00</p>-->
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-content col-md-12 text-center">
                                <img src="<?php echo $url; ?>lib/common/img/coins2pump-lite.png" alt="">
                                <p>LITECOIN</p>
                                <!--<p>0.160</p>-->
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-content col-md-12 text-center">
                                <img src="<?php echo $url; ?>lib/common/img/coins2pump-bcash.png" alt="">
                                <p>BCASH</p>
                                <!--<p>0.290</p>-->
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-content col-md-12 text-center">
                                <img src="<?php echo $url; ?>lib/common/img/coins2pump-dash.png" alt="">
                                <p>DASH</p>
                                <!--<p>0.057</p>-->
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-content col-md-12 text-center">
                                <img src="<?php echo $url; ?>lib/common/img/coins2pump-nano.png" alt="">
                                <p>NANO</p>
                                <!--<p>3.00</p>-->
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-content col-md-12 text-center">
                                <img src="<?php echo $url; ?>lib/common/img/coins2pump-users.png" alt="">
                                <p>GRUPO PRIVADO</p>
                                <!--<p>BTC 0.05</p>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** CTA Area End ***** -->

    <!-- ***** Custom Area Start ***** -->
    <section class="special-area bg-white " id="about" style="padding: 30px 0;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Heading Text -->
                    <div class="section-heading text-center">
                        <h2>A nossa sede é descentralizada.</h2>
                        <p class="subtitle">Conectamos rebeldes em qualquer lugar do mundo</p>
                        <div class="line-shape"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <img src="<?php echo $url; ?>lib/common/img/coins2pump-map.jpg" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Custom Area End ***** -->


    <!-- ***** Custom Area Start ***** -->
    <section class="special-area bg-white" id="random-img2">
        <div class="container">
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-5 col align-self-center">
                    <h3 id="mytext" style="color: #ffffff;">Conteúdo, entretenimiento e chat online grátis. Preparados para o pump?</h3>
                </div>
                <div class="col-md-7 col align-self-center">
                    <img src="<?php echo $url; ?>lib/common/img/coins2pump-section-img.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Custom Area End ***** -->

    <!-- ***** Contact Us Area Start ***** -->
    <section class="footer-contact-area section_padding_100 clearfix" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <!-- Heading Text  -->
                    <div class="section-heading">
                        <h2>Fale com a gente!</h2>
                        <div class="line-shape"></div>
                    </div>

                    <div class="email-text">
                        <p><span>Email:</span> contato@coins2pump.com</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- Form Start-->
                    <div class="contact_from">
                        <form action="#" method="post">
                            <!-- Message Input Area Start -->
                            <div class="contact_input_area">
                                <div class="row">
                                    <!-- Single Input Area Start -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" id="name" placeholder="Your Name" required>
                                        </div>
                                    </div>
                                    <!-- Single Input Area Start -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" id="email" placeholder="Your E-mail" required>
                                        </div>
                                    </div>
                                    <!-- Single Input Area Start -->
                                    <div class="col-12">
                                        <div class="form-group">
                                            <textarea name="message" class="form-control" id="message" cols="30" rows="4" placeholder="Your Message *" required></textarea>
                                        </div>
                                    </div>
                                    <!-- Single Input Area Start -->
                                    <div class="col-12">
                                        <button type="submit" class="btn submit-btn">Send Now</button>
                                    </div>
                                </div>
                            </div>
                            <!-- Message Input Area End -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Contact Us Area End ***** -->

    <?php footer($url); ?>
    <?php scripts($url); ?>
    <script>
       $(document).ready(function () {

           setTimeout(function () {
               funciones.resizeFunciona();
           }, 1000);

           var funciones = {
               resizeFunciona: function () {
                   var img = $('#img-comofunciona');
                   var p = $('.como-funciona .description-container p')
                   var height = img.height() / 2;
                   p.height(height);
               }
           }

           $(window).resize(function () {
               funciones.resizeFunciona();
           });


           $('.owl-carousel').owlCarousel({
               loop: true,
               autoplay: true,
               autoplayTimeout: 4000,
               margin: 10,
               responsiveClass: true,
               responsive: {
                   0: {
                       items: 3,
                       nav: false
                   },
                   600: {
                       items: 5,
                       nav: false
                   },
                   1000: {
                       items: 8,
                       nav: false,
                   }
               }
           })

           HideLoader();


       });
    </script>
</body>
</html>