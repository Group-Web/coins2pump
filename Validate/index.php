<?php
$url = "../";
$name = "Login";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html lang="pt">

    <head>
        <?php head($url, $name); ?>
    </head>

    <body>
        <?php loader(); ?>

        <?php menu($url, $name); ?>

        <!-- TITLE SECTION -->
        <section class="page-header clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1>Validar Usuario</h1> 
                        <div class="line-shape line-shape-center"></div> 
                    </div>
                </div>
            </div>
        </section>
        <!-- / TITLE SECTION -->

        <section class="special-area bg-white section_padding_50" id="section-login">
            <div class="div-register-mail">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-5">
                            <h3 class="text-success">¡Usuario Registrado Correctamente!</h3>
                            <div class="text-center mt-4 mb-4">
                                <i class="fa fa-envelope fa-4x" aria-hidden="true"></i>
                                <p class="mt-4">Se ha enviado un e-mail a su dirección de correo electrónico. 
                                    Compruebe su correo y valide su cuenta. Este proceso puede tardar algunos minutos. 
                                    Verifique tambien dentro de su carpeta de spam.</p>
                            </div>    
                            <div class="form-group text-center mt-4">
                                <span>Si no ha recibido el correo pulse <a id="resend_email" href="#">Aqui</a>.</span>
                                <br>
                                <span>Si ya lo recibio <a id="resend_email" href="../Login/">Inicie Session</a>.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php footer($url); ?>

        <?php scripts($url); ?>
        <script src="../lib/common/js/validate.js"></script>
        <script src="../lib/jwt/jwt-decode.min.js"></script>
        <script src="../lib/common/js/login.js"></script>
        <script>
           $(document).ready(function () {
               var url = BaseUrl + 'user/validate/';
               var ValidateUser = {
                   init: function () {
                       var id = window.location.href.split('/').pop();
//                       var id = getUrlParameter('val');
                       this.validateService(id);
                   },
                   validateService: function (id) {
                       $.ajax({
                           contentType: "application/json",
                           dataType: "json",
                           type: "GET",
                           headers: {},
                           url: BaseUrl + 'user/validate/' + id,
                           beforeSend: function (xhr) { },
                           success: function (result) {
                               GeneratePop("!Validar Usuario!", result.message, "success", HomeUrl + "Login");
                           },
                           error: function (xhr) {
                               GeneratePop("!Validar Usuario!", JSON.parse(xhr.responseText).message, "error", HomeUrl + "/Login", xhr);
                           },
                           complete: function (jqXHR, textStatus) {}
                       });
                   },
                   structureValidate: function () {
                       return  '<div class="text-center ' + TypeText + '">'
                               + '<i class="fa fa-check-circle fa-5x" aria-hidden="true"></i>'
                               + '<p class="mt-4">' + menssage + '</p>'
                               + '</div>'
                               + '<div class="form-group text-center mt-4">'
                               + '<span>' + footer + ' <a href="' + url + '">Aqui</a>.</span>'
                               + '</div>';
                   }
               };
               ValidateUser.init();
           });
        </script>
    </body>

</html>
