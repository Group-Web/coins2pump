<?php
$web_name = " Coins2Pump";
$web_url_desarrollador = "https://www.btowebsolutions.com.ve";
$web_name_desarrollador = "BTO";
$copy = "Copyright 2018 Coins2pump.";
$rif = "";
$version = "1.0.0";
$redes = array("facebook" => "#", "instagram" => "#", "twitter" => "#");

// cabecera y metas
function head($base_url, $who_is) {
   global $web_name;
   ?>
   <meta charset="UTF-8">
   <meta name="description" content="">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
   <title><?php echo $web_name . " | " . $who_is; ?> </title>
   <script type="text/javascript">
   <?php if ($who_is == "Login" || $who_is == "RecoveryPass" || $who_is == "ValidateEmail" || $who_is == "Register" || $who_is == "ChangePassword") { ?>
         if (localStorage['coins_token']) {
             window.location.href = "<?php echo $base_url; ?>Dashboard";
         }
   <?php } elseif ($who_is == "Home") { ?>

   <?php } else { ?>
         if (!localStorage['coins_token']) {
             localStorage.clear();
             window.location.href = "<?php echo $base_url; ?>Login";
         }
   <?php } ?>
   </script>
   <link rel="stylesheet" href="<?php echo $base_url; ?>lib/common/style.css">
   <link rel="stylesheet" href="<?php echo $base_url; ?>lib/common/css/responsive.css">
   <link rel="shortcut icon" type="image/x-icon" href="<?php echo $base_url; ?>lib/common/img/isotipo.png"/>

   <link rel="stylesheet" href="<?php echo $base_url; ?>lib/owlcarousel/assets/owl.carousel.min.css">
   <link rel="stylesheet" href="<?php echo $base_url; ?>lib/owlcarousel/assets/owl.theme.default.min.css">


   <?php
}

//Menu
function menu($base_url, $who_is) {
   ?>
   <header class="header_area animated">
       <div class="container-fluid">
           <div class="row align-items-center">
               <!-- Menu Area Start -->
               <div class="col-12 col-lg-10">
                   <div class="menu_area">
                       <nav class="navbar navbar-expand-lg navbar-light">
                           <!-- Logo -->
                           <a class="navbar-brand" href="<?php echo $base_url; ?>Dashboard">
                               <img class="d-block" src="<?php echo $base_url; ?>lib/common/img/coins2pump-logo.png" alt="Logo Coins2Pump"/>
                           </a>
                           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                           <!-- Menu Area -->
                           <div class="collapse navbar-collapse text-center" id="ca-navbar">
                               <ul class="navbar-nav ml-auto" id="nav">
                                   <li class="nav-item"><a class="nav-link" href="<?= $base_url; ?>#quienessomos">Sobre nós</a></li>
                                   <li class="nav-item"><a class="nav-link" href="<?= $base_url; ?>#comofunciona">Como Funciona</a></li>
   <!--                                   <li class="nav-item"><a class="nav-link" href="<?= $base_url; ?>#screenshot">Termos</a></li>
                                   <li class="nav-item"><a class="nav-link" href="<?= $base_url; ?>#groups">Grupos</a></li>
                                   <li class="nav-item"><a class="nav-link" href="<?= $base_url; ?>#testimonials">Assine</a></li>-->
                                   <li class="nav-item"><a class="nav-link" href="<?= $base_url; ?>#contact">Contato</a></li>
                               </ul>
                               <div class="sing-up-button d-lg-none text-center">
                                   <?php if ($who_is != "Login") { ?>
                                      <a href="<?= $base_url; ?>Login">Entrar</a>
                                   <?php } else { ?>
                                      <a href="<?= $base_url; ?>Register">Cadastrado</a>
                                   <?php } ?>
                               </div>
                           </div>
                       </nav>
                   </div>
               </div>
               <!-- Signup btn -->
               <div class="col-12 col-lg-2">
                   <div class="sing-up-button d-none d-lg-block">
                       <?php if ($who_is != "Login") { ?>
                          <a href="<?= $base_url; ?>Login">Entrar</a>
                       <?php } else { ?>
                          <a href="<?= $base_url; ?>Register">Cadastrado</a>
                       <?php } ?>
                   </div>
               </div>
           </div>
       </div>
   </header>
   <?php
}

function menu_login($base_url, $who_is) {
   ?>
   <header class="header_area animated">
       <div class="container-fluid">
           <div class="row align-items-center">
               <!-- Menu Area Start -->
               <div class="col-12 col-lg-10">
                   <div class="menu_area">
                       <nav class="navbar navbar-expand-lg navbar-light">
                           <!-- Logo -->
                           <a class="navbar-brand" href="<?php echo $base_url; ?>Dashboard">
                               <img class="d-block" src="<?php echo $base_url; ?>lib/common/img/coins2pump-logo.png" alt="Logo Coins2Pump"/>
                           </a>
                           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                           <!-- Menu Area -->
                           <div class="collapse navbar-collapse text-center" id="ca-navbar">
                               <ul class="navbar-nav ml-auto" id="nav">
                                   <li class="nav-item"><a class="nav-link" href="<?= $base_url; ?>Chat">Sala de Chats</a></li>
                                   <li class="nav-item"><a class="nav-link" href="<?= $base_url; ?>Dashboard/Videos">Vídeos </a></li>
                                   <li class="nav-item"><a class="nav-link" href="<?= $base_url; ?>Chat/create.php">Crie Chats</a></li>
                                   <li class="nav-item"><a class="nav-link" href="<?= $base_url; ?>Coupon/">Cupons</a></li>
                                   <li class="nav-item"><a class="nav-link" href="<?= $base_url; ?>Profile">Perfil</a></li>
                               </ul>
                               <div class="sing-up-button d-lg-none text-center">
                                   <a href="#" role="button" onclick="logout('<?= $base_url; ?>');" >Saída</a>
                               </div>
                           </div>
                       </nav>
                   </div>
               </div>
               <!-- Signup btn -->
               <div class="col-12 col-lg-2">
                   <div class="sing-up-button d-none d-lg-block">
                       <a href="#" role="button" onclick="logout('<?= $base_url; ?>');" >Saída</a>
                   </div>
               </div>
           </div>
       </div>
   </header>
   <?php
}

//Pie de pagina
function footer($base_url) {
   global $web_name_desarrollador, $web_url_desarrollador, $copy, $redes;
   ?>
   <footer class="text-center section_padding_0_50 clearfix">

       <!-- footer logo -->
       <div class="col-12 col-md-4 offset-md-4 p-5">
           <img class="" src="<?= $base_url; ?>lib/common/img/coins2pump-logo.png" alt="Logo Coins2Pump"/>
       </div>  

       <!-- social icon-->
       <div class="footer-social-icon">
           <a href="<?= $redes['facebook']; ?>"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
           <a href="<?= $redes['twitter']; ?>"><i class="fab fa-twitter" aria-hidden="true"></i></a>
           <a href="<?= $redes['instagram']; ?>"><i class="fab fa-instagram" aria-hidden="true"></i></a>
       </div>

       <div class="footer-menu">
           <nav>
               <ul>
                   <li><a href="<?= $base_url; ?>conditions.php">Termos e Condições</a></li>
                   <li><a href="<?= $base_url; ?>privacy_policy.php">Política de privacidadey</a></li>
                   <li><a href="<?= $base_url; ?>#contact">Contato</a></li>
               </ul>
           </nav>
       </div>

       <!-- Foooter-->
       <div class="copyright-text">
           <p><?php echo $copy; ?> Developed by 
               <a href="<?php echo $web_url_desarrollador; ?>" target="_blank"><?php echo $web_name_desarrollador; ?></a></p>
       </div>
   </footer>
   <?php
}

//Pie de pagina
function footerDashboard($base_url) {
   global $web_name_desarrollador, $web_url_desarrollador, $copy, $redes;
   ?>
   <footer class="text-center pt-3 pb-3 clearfix">

       <!-- social icon-->
       <div class="footer-social-icon">
           <a href="<?= $redes['facebook']; ?>"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
           <a href="<?= $redes['twitter']; ?>"><i class="fab fa-twitter" aria-hidden="true"></i></a>
           <a href="<?= $redes['instagram']; ?>"><i class="fab fa-instagram" aria-hidden="true"></i></a>
       </div>

       <div class="footer-menu">
           <nav>
               <ul>
                   <li><a href="<?= $base_url; ?>conditions.php">Termos e Condições</a></li>
                   <li><a href="<?= $base_url; ?>privacy_policy.php">Política de privacidadey</a></li>
                   <li><a href="<?= $base_url; ?>#contact">Contato</a></li>
               </ul>
           </nav>
       </div>

       <!-- Foooter-->
       <div class="copyright-text">
           <p><?php echo $copy; ?> Developed by 
               <a href="<?php echo $web_url_desarrollador; ?>" target="_blank"><?php echo $web_name_desarrollador; ?></a></p>
       </div>
   </footer>
   <?php
}

//Cargando
function loader() {
   ?>
   <!--Logo cargando-->
   <div id="preloader">
       <div class="colorlib-load"></div>
   </div>
   <!--/Logo Cargando-->
   <?php
}

//javascripts comunes
function scripts($base_url) {
   ?>	
   <script src="<?php echo $base_url; ?>lib/theme/js/jquery-2.2.4.min.js"></script>
   <!--<script src="<?php echo $base_url; ?>lib/jQuery/jquery-3.2.1.min.js"></script>-->
   <script src="<?php echo $base_url; ?>lib/theme/js/popper.min.js"></script>
   <script src="<?php echo $base_url; ?>lib/bootstrap-4.0.0/js/bootstrap.min.js"></script>
   <script src="<?php echo $base_url; ?>lib/bootstrap-4.0.0/js/bootstrap.bundle.min.js"></script>
   <script src="<?php echo $base_url; ?>lib/SweetAlert/sweetalert.js"></script>
   <script src="<?php echo $base_url; ?>lib/theme/js/plugins.js"></script>
   <script src="<?php echo $base_url; ?>lib/theme/js/slick.min.js"></script>
   <script src="<?php echo $base_url; ?>lib/theme/js/footer-reveal.min.js"></script>
   <script src="<?php echo $base_url; ?>lib/theme/js/active.js"></script>
   <script src="<?php echo $base_url; ?>lib/common/js/BaseUrl.js"></script>
   <script src="<?php echo $base_url; ?>lib/common/js/custom.js"></script>
   <script src="<?php echo $base_url; ?>lib/owlcarousel/owl.carousel.js"></script>
   <?php
}
?>
