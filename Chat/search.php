<?php
$url = "../";
$name = "Chat";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html lang="pt">

    <head>
        <?php head($url, $name); ?>
        <link rel="stylesheet" href="../lib/FlipClock/flipclock.css">
    </head>

    <body>
        <?php // loader(); ?>

        <?php menu_login($url, $name); ?>
        
        <style>
            .page-content h2 {
                margin-bottom: 25px;
            }
        </style>

        <!-- ***** Page Header ***** -->
        <section class="page-header clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                      <h1>Chat List</h1> 
                      <div class="line-shape" style="margin-left: calc(50% - 40px);"></div> 
                    </div>
                    <div class="col-8 offset-md-2 mt-3">
                        <div class="container">
                            <div class="row">
                                <div class="col-10">
                                    <input id="search" type="text" class="form-control mb-8" placeholder="Search ...">
                                </div>
                                <div class="col-2">
                                    <button id="searchButton" class="btn btn-theme small"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Page Header ***** -->
        <!-- ***** Page Header ***** -->
        <section class="page-content clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1 text-left">
                       <div id="accordionSearch" class="search">
                          
                            

                       </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Page Header ***** -->

        <?php scripts($url); ?>
        <script type="text/javascript" src="../lib/FlipClock/flipclock.min.js"></script>
        <script type="text/javascript" src="../lib/gos/autobahn/autobahn.min.js"></script>
        <script type="text/javascript" src="../lib/gos/ws_client/gos_web_socket_client.js"></script>
        <!--<script type="text/javascript" src="../lib/common/js/chat.js"></script>-->
        <script>
            var UrlChat = BaseUrl + 'chat/';
            var UrlChatSearch = UrlChat + 'search/';
            var UrlGetInChat = UrlChat + 'in/';
            
            var Search = {
                init: function() {
                    var that = this;
                    this.searchButton =  $('#searchButton');
                    this.input =  $('#search');
                    this.accordion =  $('#accordionSearch');
                    
                    this.searchButton.on('click', function(event) {
                        event.preventDefault();
                        that.request($('input#search').val());
                    });

                    that.request('*');
                  
                },
                request: function(val) {
                    var that = this;
                    $.ajax({
                        async: true,
                        crossDomain: true,
                        contentType: "application/json",
                        dataType: "json",
                        headers: {"Authorization":  1},
                        type: "GET",
                        url: UrlChatSearch+val,
                        beforeSend: function (xhr) {
                        },
                        success: function (result) {
                            var data = result.data;
                            console.log(data);
                            that.accordion.html('');
                            $.each(data, function (i, item) {
                                that.accordion.append(that.itemStructure(item));
                                $('#accordionSearch').on('click', '#addChat'+item.id, function(event) {
                                    event.preventDefault();
                                    that.getIn(item);
                                    $(this).addClass('btn-disabled').attr('disabled','disabled').html('Added');
                                });
                            });
                        },
                        error: function (xhr) {
                            //GeneratePopUpAmigo("error", "Error cargando los paises");
                            console.log(xhr);
                        },
                        complete: function (jqXHR, textStatus) {
                        }
                    });
                }, 
                itemStructure: function(item) {
                    var that = this;

                    if (item.type == 'premium') {
                        var faicon = '<span class="premium"><i class="fas fa-certificate"></i> Premium</span>';
                    }else{
                        var faicon = '<span class="free"><i class="fas fa-comments"></i> Free</span>';
                    }
                    if (item.isvalid == 1) {
                        var isvalid = '<span class="isvalid"><i class="fas fa-check"></i> Subscribed</span>';
                    }else if (item.isowner == 1) {
                        var isvalid = '<span class="isvalid"><i class="fas fa-check"></i> Owner</span>';
                    }else {
                        var isvalid = '';
                    }
                    
                    var price ='';
                    if (item.type != 'free') {
                        $.each(item.price, function (i, priceItem) { 
                            price += '<p>'+priceItem.currency+': '+priceItem.value+'</p>'; 
                        });
                    }else{
                        price = '<p>Free !</p>';
                    }
                    if ( ((item.isvalid == 1) || (item.type == 'free') || (item.isowner == 1)) ) {
                        var buttonAdd = '<button class="btn btn-theme small" id="addChat'+item.id+'">Subscribe</button>';
                    }else if ( ((item.isvalid != 1) && (item.isowner != 1)) ){
                        var buttonAdd = '<a class="btn btn-theme small" href="comprar.php" target="_blank">Buy Now</a>';
                    }/*else if (($('#chats-container #'+item.id).length != 0)){
                        var buttonAdd = '<button class="btn btn-theme btn-disabled small" disabled>Added</button>';
                    }*/
                    console.log(item.name);
                    return '<div class="card">' +
                                '<div class="card-header " id="'+item.id+'">' +
                                  '<h5 class="mb-0">' +
                                    '<div class="title-container collapsed" data-toggle="collapse" data-target="#collapse'+item.id+'" aria-expanded="false" aria-controls="collapse'+item.id+'">' +
                                        faicon +
                                        '<h4 class="box-title">'+item.name+'</h4>&nbsp;' + isvalid +
                                    '</div>' +
                                  '</h5>' +
                                '</div>' +
                                '<div id="collapse'+item.id+'" class="collapse" aria-labelledby="'+item.id+'" data-parent="#accordionSearch">' +
                                  '<div class="card-body">' +
                                    '<div class="container">' +
                                        '<div class="row">' +
                                            '<div class="description col-md-6">' +
                                                '<div class="user row">' +
                                                    '<div class="user-img col-md-3">' +
                                                        '<img src="'+item.userphoto+'" alt="User image">' +
                                                    '</div>' +
                                                    '<div class="user-info col-md-7 align-self-center">' +
                                                        '<h4>'+item.user+'</h4>' +
                                                        '<small>Something about the user.</small>' +
                                                    '</div>' +
                                                '</div>' +
                                                '<div class="topics row">' +
                                                    '<div class="col-md-12 text-center">' +
                                                        '<h4>Topics</h4><div class="line-shape"></div>' +
                                                        '<p>'+item.description+'</p>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                            '<div class="description text-center col-md-6">' +
                                                '<div class="col-md-12 text-center date">' +
                                                    '<h4>Date</h4>' +
                                                    '<p><span>Start: </span>'+item.start+'</p>' +
                                                    '<p><span>End: </span>'+item.end+'</p>' +
                                                '</div>' +
                                                '<div class="clearfix"></div>' +
                                                '<div class="col-md-12 text-center pricing">' +
                                                    '<h4>Pricing</h4>' +
                                                        price+
                                                    '<div class="col-md-6 offset-md-3">' +
                                                        buttonAdd +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                  '</div>' +
                                '</div>' +
                              '</div>';
                },
                getIn: function (item) {
                    console.log(item);
                    $.ajax({
                        async: true,
                        crossDomain: true,
                        contentType: "application/json",
                        dataType: "json",
                        headers: {"Authorization": "1"},
                        type: "POST",
                        data: JSON.stringify(item),
                        url: UrlGetInChat,
                        beforeSend: function (xhr) {
                        },
                        success: function (result) {
                            var data = result.data;
                            console.log(result);
                            //Chat.structureChat(item, $('#chats-container'));
                            //Chat.connectSession(item);
                        },
                        error: function (xhr) {
                            //GeneratePopUpAmigo("error", "Error cargando los paises");
                            console.log("Error",xhr);
                        },
                        complete: function (jqXHR, textStatus) {
                        }
                    });
                }
            }


            $(document).ready(function() {
                
                Search.init();
                
            });


        </script>
    </body>

</html>
