<?php
$url = "../";
$name = "CreateChat";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html lang="pt">

    <head>
        <?php head($url, $name); ?>
        <link rel="stylesheet" href="../lib/bootstrap-table/bootstrap-table.min.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />
    </head>

    <body>
        <?php loader(); ?>

        <?php menu_login($url, $name); ?>

        <!-- ***** Page Header ***** -->
        <section class="page-header clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-6 float-left">
                        <h1>Creador de Chats</h1> 
                        <div class="line-shape"></div> 
                    </div>
                    <div class="col-6 float-right text-right">                         
                        <button id="createNewChat" class="btn btn-theme">
                            <i class="fas fa-plus"></i> &nbsp; Add Chat
                        </button>
                    </div>
                </div>          
        </section>
        <!-- ***** Page Header ***** -->

        <section class="special-area bg-white section_padding_50" >

            <div class="div-create">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Name Chat</label>
                                <input type="text" class="form-control input-new" id="nameChat" placeholder="Nombre del chat" required="" autocomplete="off">
                                <div class="div-validador">
                                    <div>&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Cryptocurrency</label>
                                <select class="form-control input-new-c cryptocurrency" id="cryptocurrency" ></select>
                                <div class="div-validador">
                                    <div>&nbsp;</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Inicio del Chat</label>
                                <input type="text" class="form-control datetimepicker-input input-new" id="startDateChat" data-toggle="datetimepicker" data-target="#startDateChat" placeholder="date"/>
                                <!--<input type="datetime-local" class="form-control input-new" id="startDateChat" placeholder="startDateChat" required="" autocomplete="off">-->
                                <div class="div-validador">
                                    <div>&nbsp;</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Fin del Chat</label>
                                <input type="text" class="form-control datetimepicker-input input-new" id="endDateChat" data-toggle="datetimepicker" data-target="#endDateChat" placeholder="date"/>
                                <!--<input type="datetime-local" class="form-control input-new" id="endDateChat" placeholder="endDateChat" required="" autocomplete="off">-->
                                <div class="div-validador">
                                    <div>&nbsp;</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-10">
                            <label>Description Chat</label>
                            <div class="form-group">
                                <textarea type="text" class="form-control input-new" id="descriptionChat" required=""></textarea>
                                <div class="div-validador">
                                    <div>&nbsp;</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-10 mt-30 mb-5 text-center">
                            <button id="add_payment_type" type="submit" class="btn btn-outline-primary mb-5">
                                <i class="fas fa-plus"></i> &nbsp; Add metodo pago
                            </button>
                            <table id="priceChatTable" class="table table-striped" ></table>
                            <div id="priceChatTableLegend"></div>
                        </div>

                        <div class="col-md-10">
                            <button id="addChatNew" type="submit" class="btn submit-btn">Crear chat</button>
                        </div>
                    </div>
                </div>
            </div>

            <div id="div-list">
                <div class="container">
                    <h3 class="text-center">Mis Chats Creados</h3>
                    <br>
                    <table id="listChats" class="table table-striped" ></table>
                </div>
            </div> 
        </section>
        <!-- ***** Page Header ***** -->
        <?php footerDashboard($url); ?>
        <?php scripts($url); ?>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
        <script src="../lib/bootstrap-table/bootstrap-table.min.js"></script>
        <script src="../lib/common/js/create_chat.js"></script>
    </body>
</html>
