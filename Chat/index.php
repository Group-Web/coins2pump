<?php
$url = "../";
$name = "Chat";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html lang="pt">

    <head>
        <?php head($url, $name); ?>
        <link rel="stylesheet" href="../lib/FlipClock/flipclock.css">
    </head>

    <body>
        <?php loader(); ?>   

        <?php menu_login($url, $name); ?>

        <!-- ***** Page Header ***** -->
        <section class="page-header clearfix">

            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-6 float-left">
                        <h1>Chat Room</h1> 
                        <div class="line-shape"></div> 
                    </div>
                    <div class="col-6 col-md-3 float-right text-right">
                        <button class="btn btn-theme" data-toggle="modal" data-target="#addChat">
                            <i class="fas fa-plus"></i> &nbsp; Subscribe Chat
                        </button>

                        <!-- The Modal -->
                        <div class="modal fade" id="addChat">
                            <div class="modal-dialog  modal-lg" id="searchModal">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div class="container">
                                            <div class="row">
                                                <div class="title col-md-12 text-center">
                                                    <h4 class="modal-title">Chat Search</h4>
                                                    <div class="line-shape"></div>
                                                </div>
                                                <div class="col-8 col-md-10">
                                                    <input id="search" type="text" class="form-control mb-8" placeholder="Search ...">
                                                </div>
                                                <div class="col-4 col-md-2">
                                                    <button id="searchButton" class="btn btn-theme small"><i class="fas fa-search"></i></button>
                                                </div>
                                                <div class="col-12 col-md-10 form-inline searchFilter">
                                                    <!--<div class="form-check col-md-4 text-center">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" name="paidOutCheck"> Paid Out Chats
                                                        </label>
                                                    </div>-->
                                                    <div class="form-check col-6 text-center">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input filter" type="checkbox" name="free" id="freeChatCheck"> Free Chats
                                                        </label>
                                                    </div>
                                                    <div class="form-check col-6 text-center">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input filter" type="checkbox" name="premium" id="premChatCheck"> Premium Chats
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-body">
                                        <div class="container">
                                            <div class="row"><div class="col-md-12 text-left">  
                                                    <div id="accordionSearch" class="search">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- The Modal -->
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Page Header ***** -->
        <!-- ***** Page Header ***** -->
        <section class="page-content clearfix">

            <div class="container">
                <div class="row">
                    <div class="col-12 text-center" id="chats-container">
                        <div id="emptyRoom">
                            <h2>You have not added chats yet.</h2> 
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Page Header ***** -->

        <?php footerDashboard($url); ?>

        <?php scripts($url); ?>
        <script type="text/javascript" src="../lib/highcharts/highcharts.js"></script>
        <script type="text/javascript" src="../lib/FlipClock/flipclock.min.js"></script>
        <script type="text/javascript" src="../lib/gos/autobahn/autobahn.min.js"></script>
        <script type="text/javascript" src="../lib/gos/ws_client/gos_web_socket_client.js"></script>
        <script type="text/javascript" src="../lib/common/js/chat.js"></script>
    </body>
</html>