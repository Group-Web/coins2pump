<?php
$url = "../";
$name = "CreateChat";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html lang="pt">

    <head>
        <?php head($url, $name); ?>
        <link rel="stylesheet" href="../lib/bootstrap-table/bootstrap-table.min.css">
        <style>
            .fixed-table-pagination .pagination-info {
                margin-left: 10px;
            }
        </style>
    </head>

    <body>
        <?php // loader(); ?>
        <?php menu_login($url, $name); ?>

        <!-- ***** Page Header ***** -->
        <section class="page-header clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-6 float-left ">
                        <h1>Mis Cupones</h1> 
                        <div class="line-shape"></div> 
                    </div>
                    <div class="col-6 float-right text-right">                         
                        <button id="buyCoupon" class="btn btn-theme">
                            <i class="fas fa-plus"></i> &nbsp; Comprar Cupon
                        </button>
                    </div>
                </div>          
        </section>
        <!-- ***** Page Header ***** -->
        <section class="special-area bg-white section_padding_50" >
            <div id="div-list">
                <div class="container buy-coupon" style="display: none">
                    <h3 class="text-center">Comprar un cupon</h3>
                    <br>
                    <table id="listChats" class="table table-striped" ></table>
                </div>
                <div class="container pb-2 pt-5">
                    <h3 class="text-center">Mis cupones</h3>
                    <br>
                    <table id="listMyCoupons" class="table table-striped" ></table>
                    <div class="pt-4 text-right">
                        <i title="Usado" class="text-danger fas fa-times"></i> Cupon Usado &nbsp;&nbsp;
                        <i title="Validar Pago" class="text-success fas fa-check"></i> Cupon Disponible&nbsp;&nbsp;
                        <i title="Validar Pago" class="text-discconnected fas fa-check"></i> Pendiente Validar Pago&nbsp;&nbsp;
                    </div>

                </div>
            </div>
        </section>
        <!-- ***** Page Header ***** -->
        <?php scripts($url); ?>
        <script src="../lib/bootstrap-table/bootstrap-table.min.js"></script>
        <script type="text/javascript">
           var CreateCoupon = {
               init: function () {
                   var that = this;
                   localStorage.removeItem('idChat');
                   localStorage.removeItem('idService');
                   this.requestMyListCoupon();
                   // Eventos
                   $('#buyCoupon').on('click', function (e) {
                       that.requestListBuyCoupon();
                   });
                   $('#listChats').on('click', '.payment_coupon', function (e) {
                       localStorage["idChat"] = null;
                       localStorage["idService"] = $(this).children('.payment_coupon').val();
                       window.location.href = "../Dashboard/Payment/";
                   });
               },
               requestListBuyCoupon: function () {
                   var url = BaseUrl + 'chat/coupon/buylist/';
                   $.ajax({
                       async: true,
                       crossDomain: true,
                       contentType: "application/json",
                       dataType: "json",
                       headers: {"Authorization": localStorage['coins_token']},
                       type: "GET",
                       url: url,
                       beforeSend: function (xhr) {
                       },
                       success: function (result) {
                           $("#listChats").bootstrapTable("destroy");
                           $('#listChats').bootstrapTable({
                               columns: [
                                   {field: 'nameServicePayment', title: 'Nombre'},
                                   {field: 'descriptionServicePayment', title: 'Descripción'},
                                   {
                                       field: 'idServicePayment',
                                       title: 'Comprar',
                                       align: 'center',
                                       formatter: function (value) {
                                           return '<a class="payment_coupon" data-toggle="tooltip" title="Pagamento">' +
                                                   '<input class="payment_coupon" value="' + value + '" hidden/>' +
                                                   '<i class="text-primary far fa-credit-card"></i></a>';
                                       }
                                   }
                               ],
                               data: result.data,
                               pagination: false,
                               search: false,
                               showToggle: false
                           });
                           $(".buy-coupon").fadeIn("slow");
                       },
                       error: function (xhr) {
                           GeneratePop("¡Cupones!", "", "error", HomeUrl + "Dashboard", xhr);
                       },
                       complete: function (jqXHR, textStatus) {
                       }
                   });
               },
               requestMyListCoupon: function () {
                   var url = BaseUrl + 'chat/coupon/mylist/';
                   $.ajax({
                       async: true,
                       crossDomain: true,
                       contentType: "application/json",
                       dataType: "json",
                       headers: {"Authorization": localStorage['coins_token']},
                       type: "GET",
                       url: url,
                       beforeSend: function (xhr) {
                       },
                       success: function (result) {
                           $("#listMyCoupons").bootstrapTable("destroy");
                           $('#listMyCoupons').bootstrapTable({
                               columns: [
                                   {field: 'nameServicePayment', title: 'Nombre'},
                                   {
                                       field: 'nameChat',
                                       title: 'Chat',
                                       formatter: function (value) {
                                           if (!value) {
                                               return "No Asignado";
                                           }
                                           return value;
                                       }},
                                   {field: 'createdPayment', title: 'Fecha Comprado'},
                                   {
                                       field: 'idChat',
                                       title: 'Disponibilidad',
                                       align: 'center',
                                       formatter: function (value, row) {
                                           if (value) {
                                               return '<i title="Usado" class="text-danger fas fa-times"></i>';
                                           } else if (row.validPayment == 0) {
                                               return '<i title="Validar Pago" class="text-discconnected fas fa-check"></i>';
                                           } else {
                                               return '<i title="Disponible" class="text-success fas fa-check"></i>';
                                           }
                                       }
                                   }
                               ],
                               data: result.data,
                               pagination: true,
                               search: true,
                               showToggle: false
                           });
                       },
                       error: function (xhr) {
                           GeneratePop("¡Cupones!", "", "error", HomeUrl + "Dashboard", xhr);
                       },
                       complete: function (jqXHR, textStatus) {
                       }
                   });
               }
           };
           $(document).ready(function () {
               CreateCoupon.init();
           });
        </script>
    </body>
</html>
