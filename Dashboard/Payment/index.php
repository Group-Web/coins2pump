<?php
$url = "../../";
$name = "Payment";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html lang="pt">

    <head>
        <?php head($url, $name); ?>
        <style>
            #header_tabs li a img{
                width: 35px;
                height: 35px;

            }
            #header_tabs.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
                color: #495057;
                background-color: #fff;
                border:none;
                border-radius: 0px;
                border-bottom: 3px solid #02EA02;
            }
            #header_tabs.nav-tabs {
                padding: 3px 0;
                border-bottom: 0px solid #fff;
            }
            #header_tabs .nav-link:hover {
                border-radius: 0px;
                border-width: 0px 0px 3px;
                border-color: #fff #fff #02EA02;
            }
            #content_tabs p{
                color:#666666;
            }
            #content_tabs span{
                color:#000;
            }
        </style>
    </head>

    <body>
        <?php loader(); ?>

        <?php menu_login($url, $name); ?>

        <!-- TITLE SECTION -->
        <section class="page-header clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1>Payment</h1> 
                        <div class="line-shape line-shape-center"></div> 
                    </div>
                </div>
            </div>
        </section>
        <!-- / TITLE SECTION -->

        <!-- ***** Page Header ***** -->
        <section class="special-area bg-white section_padding_50" id="section-register">
            <div class="container">
                <div class="row justify-content-center">

                    <div class="col-md-6">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12">
                                    <div id="infoPayment"></div>
                                </div>

                                <div class="col-12">
                                    <ul id="header_tabs" class="nav nav-tabs nav-justified" role="tablist"></ul>
                                </div>
                                
                                <div id="content_tabs" class="col-md-12 pt-5 pb-3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div id="qrcode"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <p><label>Precio:&nbsp;</label> <span id="valuePayment"></span></p>
                                            <p><label>Direccion de Pago:&nbsp;</label><ins><span id="strcode"></span></ins></p>
                                            <div class="form-group div-va">
                                                <label>Numero de Referencia:</label>
                                                <input type="text" class="referencePayment form-control input-new" id="referencePayment" placeholder="Numero de referencia" required="" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-theme " id="comprar">
                                <i class="fas fa-plus"></i> &nbsp; Confirmar Compra
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- ***** Page Header ***** -->
        <?php footerDashboard($url); ?>

        <?php scripts($url); ?>
        <script src="../../lib/common/js/jquery-qrcode-0.14.0.min.js"></script>
        <script type="text/javascript" src="../../lib/common/js/payment.js"></script>
    </body>
</html>