<?php
$url = "../../";
$name = "Videos";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html lang="pt">

    <head>
        <?php head($url, $name); ?>
        <style>
            #video-embed{
                padding: 5% 0 7%;
            }
            .video-wrap{
                background: #F4F4F4;
                padding: 5%;
                border:2px solid #5b1966;
                border-radius: 24px 24px 24px 0px;
                margin-bottom: 20px;
                min-height: 240px;
            }
            .video-wrap:hover{
                background: #dbd9d9;
                border-color:#02EA02;
                cursor: pointer;
            }
            .video-container2 p{
                font-weight: bold;
                margin-top: 5px;
                font-size: 14px;
            }
        </style>
    </head>

    <body>
        <?php loader(); ?>

        <?php menu_login($url, $name); ?>

        <!-- TITLE SECTION -->
        <section class="page-header clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-6 float-left">
                        <h1>Ultimos Videos</h1> 
                        <div class="line-shape"></div> 
                    </div> 
                    <div class="col-6 col-md-3 float-right text-right">
                        <a class="btn btn-theme pt-2" href="">Mis Videos </a>
                    </div>
                </div>
            </div>
        </section>
        <!-- / TITLE SECTION -->

        <!-- ***** Page Header ***** -->
        <section class="special-area bg-white section_padding_50" >
            <div class="container">
                <div id="info-container2" class="row">
                </div>
            </div>
        </section>
        <!-- ***** Page Header ***** -->

        <?php footerDashboard($url); ?>

        <?php scripts($url); ?>
        <script type="text/javascript" src="../../lib/common/js/profile.js"></script>
        <script>
            $(document).ready(function () {
                ViewVideos.request();
            });
        </script>
    </body>

</html>
