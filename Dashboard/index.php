<?php
$url = "../";
$name = "Dashboard";
include $url . 'common.php';
?>
<!DOCTYPE html>
<html lang="pt">

    <head>
        <?php head($url, $name); ?>
        <style>
            .div-operations{
                background: rgb(64,26,65); /* Old browsers */
                background: -moz-linear-gradient(left, rgba(64,26,65,1) 0%, rgba(91,25,102,1) 47%, rgba(91,25,102,1) 55%, rgba(91,25,102,1) 55%, rgba(64,26,65,1) 100%); /* FF3.6-15 */
                background: -webkit-linear-gradient(left, rgba(64,26,65,1) 0%,rgba(91,25,102,1) 47%,rgba(91,25,102,1) 55%,rgba(91,25,102,1) 55%,rgba(64,26,65,1) 100%); /* Chrome10-25,Safari5.1-6 */
                background: linear-gradient(to right, rgba(64,26,65,1) 0%,rgba(91,25,102,1) 47%,rgba(91,25,102,1) 55%,rgba(91,25,102,1) 55%,rgba(64,26,65,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#401a41', endColorstr='#401a41',GradientType=1 ); /* IE6-9 */
                border:2px solid #5b1966;
                border-radius: 24px 24px 24px 0px;
                color:#fff;
                font-size: 20px;
                padding: 50px 10px;
                margin: 10px 0;
            }
            .div-operations:hover{
                border: 2px solid #02EA02;
                color:#02EA02;
                opacity: 0.85;
            }
            .div-operations i{
                font-size:50px;
            }
        </style>
    </head>

    <body>
        <?php loader(); ?>

        <?php menu_login($url, $name); ?>

        <!-- TITLE SECTION -->
        <section class="page-header clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1>Welcome To Coins2pump</h1> 
                        <div class="line-shape line-shape-center"></div> 
                    </div>
                </div>
            </div>
        </section>
        <!-- / TITLE SECTION -->

        <!-- ***** Page Header ***** -->
        <section class="special-area bg-white section_padding_50" >
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3>Te damos la bienvenida a nuestro portal, para implificar tu tarea creamos estos accesos directos</h3>
                        <div class="container-fluid">
                            <div class="row justify-content-center mt-5">
                                <div class="col-sm-5 text-center">
                                    <a href="Videos/">
                                        <div class="div-operations">
                                            <div class="div-operations-icon">
                                                <i class="fab fa-youtube"></i>
                                            </div>
                                            <div class="div-operations-text">
                                                <span>Videos de Youtube</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-5 text-center">
                                    <a href="../Chat/">
                                        <div class="div-operations">
                                            <div class="div-operations-icon">
                                                <i class="far fa-comments"></i>
                                            </div>
                                            <div class="div-operations-text">
                                                <span>Salas de chat</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-5 text-center">
                                    <a href="../Chat/create.php">
                                        <div class="div-operations">
                                            <div class="div-operations-icon">
                                                <i class="far fa-comment"></i>
                                            </div>
                                            <div class="div-operations-text">
                                                <span>Create new chat</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-5 text-center">
                                    <a href="../Coupon/">
                                        <div class="div-operations">
                                            <div class="div-operations-icon">
                                                <i class="far fa-comment"></i>
                                            </div>
                                            <div class="div-operations-text">
                                                <span>Comprar Cupones</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-5 text-center">
                                    <a href="../Profile/">
                                        <div class="div-operations">
                                            <div class="div-operations-icon">
                                                <i class="far fa-comment"></i>
                                            </div>
                                            <div class="div-operations-text">
                                                <span>Mi Perfil</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-5 text-center">
                                    <a href="#">
                                        <div class="div-operations">
                                            <div class="div-operations-icon">
                                                <i class="far fa-question-circle"></i>
                                            </div>
                                            <div class="div-operations-text">
                                                <span>Dudas sobre a web</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Page Header ***** -->
        <?php footerDashboard($url); ?>
        <?php scripts($url); ?>
        <script>
           $(document).ready(function () {
                HideLoader();
           });
        </script>
    </body>
</html>