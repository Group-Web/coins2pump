<?php
$url = "../";
$name = "Profile";
include $url . 'common.php';
if (isset($_GET['user'])) {
   $userUrl = '/' . $_GET['user'];
} else {
   $userUrl = '';
}
?>
<!DOCTYPE html>
<html lang="pt">

    <head>
        <?php head($url, $name); ?>
        <link rel="stylesheet" href="../lib/FlipClock/flipclock.css">
    </head>

    <body>
        <?php // loader(); ?>

        <?php menu_login($url, $name); ?>

        <!-- ***** Page Header ***** -->
        <section class="page-header clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-9 float-left">
                        <h1>Profile</h1> 
                        <div class="line-shape"></div> 
                    </div> 
                    <div class="col-3 float-right text-right">
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Page Header ***** -->
        <!-- ***** Page Header ***** -->
        <section class="page-content clearfix">
            <div class="container" id="profile-container">
                <div class="row">
                    <style>

                    </style>
                    <div class="profile col-12 col-md-3 mb-3">
                        <img src="http://www.thehindu.com/sci-tech/technology/internet/article17759222.ece/alternates/FREE_660/02th-egg-person" alt="User image">
                        <div id="profile-info">
                            <h4 class="name" id="name"></h4>
                            <h4 class="info" id="username"></h4>
                            <h4 class="info" id="email"></h4>
                            <h4 class="info" id="country"></h4>
                            <hr>
                            <h4 class="">About me</h4>
                            <p id="description"></p>
                            <hr>
                        </div>

                        <div class="profile-form" style="display: none;">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name"></input>
                            </div>
                            <div class="form-group">
                                <label for="lastname">LastName</label>
                                <input type="text" class="form-control" id="lastname"></input>
                            </div>
                            <!--<div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" id="username"></input>
                            </div>-->
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email"></input>
                            </div>
                            <div class="form-group">
                                <label for="country">Country</label>
                                <select class="form-control" id="country">
                                    <option>Venezuela</option>
                                    <option>Brasil</option>
                                    <option>USA</option>
                                </select>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" id="description" rows="5"></textarea>
                            </div>
                            <hr>
                            <button class="btn btn-theme" id="sendEdit">Update</button>
                        </div>
                    </div>
                    <hr>
                    <div class="col-12 col-md-8 offset-md-1 text-justify">
                        <div class="container"  id="info-container">

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Page Header ***** -->

        <?php footerDashboard($url); ?>
        <?php scripts($url); ?>
        <script type="text/javascript" src="../lib/FlipClock/flipclock.min.js"></script>
        <script type="text/javascript" src="../lib/gos/autobahn/autobahn.min.js"></script>
        <script type="text/javascript" src="../lib/gos/ws_client/gos_web_socket_client.js"></script>
        <script type="text/javascript" src="../lib/common/js/profile.js"></script>
        <script>
           var userProfile = '<?= $userUrl ?>';
           var UrlProfile = BaseUrl + 'user/profile<?= $userUrl ?>';
           $(document).ready(function () {
               var profile = Profile.request();
           });
        </script>
    </body> 

</html>
